# crm-dou-api

#### 介绍

抖店API简单封装

#### 使用说明

##### 使用SpringBoot框架，在yml中配置抖店API密钥

```java
    # 抖音api配置
    dou-yin:
        app-key:123456
        app-secret:abcdefg
        version:2
        app-host:https://openapi-fxg.jinritemai.com
        is-self-use:true
```
#### 增加接口
请求接口封装请继承 BaseDouYinRequest类，请求响应继承 BaseDouYinResponse

#### 调用接口
   ```java
   @Autowired
   private DouYinClient douYinClient;
   
   @Test
   void getApi() throws ApiException {
       OrderListRequest request = new OrderListRequest();
       request.setStartTime(LocalDateTime.now().plusDays(-10));
       request.setEndTime(LocalDateTime.now());
       request.setOrderBy(OrderListRequest.ORDER_BY_FIELD_CREATE);
       request.setPage("1");
       request.setSize("10");
   
       OrderListResponse execute = douYinClient.execute(request, "xxxxxxxx");
       assertEquals(execute.getErrNo(), 0);
       System.out.println(execute.getData().getTotal());
   }
   ```
#### 版本迭代
v1.2.0
根据抖音官方修改，自研应用允许多个店铺授权，已跟工具类应用区别不大。
如果自研只授权了一个店铺，建议设置`is-self-use:true`
数据加/解密接口更新

v1.1.0 增加了 `/order/searchList` 和 `/order/orderDetail`
废弃原`/order/list` 和 `/order/detail`

v1.0.0 增加了自研应用和isv应用的客户端  
自研应用无须维护token，系统自动内置刷新和维护机制




#### 参与贡献

1. Fork 本仓库
2. 新建 feature/xxx 分支
3. 提交代码
4. 新建 Pull Request

