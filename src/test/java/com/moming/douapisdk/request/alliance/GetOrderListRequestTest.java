package com.moming.douapisdk.request.alliance;

import com.moming.douapisdk.ApiException;
import com.moming.douapisdk.client.DouYinClient;
import com.moming.douapisdk.response.alliance.GetOrderListResponse;
import org.assertj.core.util.Lists;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import static org.junit.jupiter.api.Assertions.*;

/**
 * @author lujingpo
 * @date 2021/12/13
 */
@SpringBootTest
@ActiveProfiles(value = "local")
class GetOrderListRequestTest {

    @Autowired
    private DouYinClient douYinClient;

    // private final String accessToken = "2ba2eb8a-cd8d-4521-9a9c-fbe1916970e2";

    @Test
    void testExecute() throws ApiException {
        GetOrderListRequest getOrderListRequest = new GetOrderListRequest();
        getOrderListRequest.setOrderIds(Lists.newArrayList("4941632647441194771"));
        GetOrderListResponse execute = douYinClient.execute(getOrderListRequest);
        System.out.println(execute.getDatas());
    }
}