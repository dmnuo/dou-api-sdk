package com.moming.douapisdk.request;

import com.moming.douapisdk.ApiException;
import com.moming.douapisdk.client.DefaultDouYinClient;
import com.moming.douapisdk.client.DouYinClient;
import com.moming.douapisdk.response.OrderDetailNewResponse;
import com.moming.douapisdk.response.OrderDetailResponse;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import static org.junit.jupiter.api.Assertions.*;

/**
 * @author lujingpo
 * @date 2021/6/12
 */
@SpringBootTest
@ActiveProfiles(value = "local")
class OrderDetailNewRequestTest {

    @Autowired
    private DouYinClient douYinClient;

    private final String accessToken = "c5b8d33e-e037-44e6-97df-408581f184e3";


    @Test
    void testApi() throws ApiException {
        //4807770641499757618
        OrderDetailNewRequest request = new OrderDetailNewRequest();

        request.setShopOrderId("6918327019045459313");

        OrderDetailNewResponse execute = douYinClient.execute(request);

        assertNotNull(execute);
    }

    @Test
    void testOldApi() throws ApiException {
        OrderDetailRequest request = new OrderDetailRequest();
        request.setMainOrderId("4806704231194147903");
        OrderDetailResponse execute = douYinClient.execute(request, accessToken);

        assertNotNull(execute);
    }
}