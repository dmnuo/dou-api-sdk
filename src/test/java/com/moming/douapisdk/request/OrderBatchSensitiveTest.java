package com.moming.douapisdk.request;

import com.moming.douapisdk.ApiException;
import com.moming.douapisdk.client.DouYinClient;
import com.moming.douapisdk.domain.CipherInfo;
import com.moming.douapisdk.response.OrderBatchSensitiveResponse;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import java.util.*;

import static org.junit.jupiter.api.Assertions.assertNotNull;

/**
 * 批量脱敏测试
 * @author LangYu
 * @date 2021/7/10
 */

@SpringBootTest
@ActiveProfiles(value = "local")
@ConditionalOnProperty(prefix = "spring.dou-yin", name = "is-self-use", havingValue = "true")
public class OrderBatchSensitiveTest {

    @Autowired
    private DouYinClient douYinClient;


    private final String accessToken = "edae7c30-8386-443b-88a1-031111596fdd";

    @Test
    void test() throws ApiException {
        CipherInfo c = new CipherInfo();
        c.setAuthId("123");
        c.setCipherText("asdasda");
        List<CipherInfo> list = new ArrayList<CipherInfo>();
        list.add(c);
        OrderBatchSensitiveRequest request = new OrderBatchSensitiveRequest();
        request.setCipherInfoList(list);

        OrderBatchSensitiveResponse execute = douYinClient.execute(request, accessToken);
        assertNotNull(execute);
    }
}
