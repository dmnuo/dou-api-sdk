package com.moming.douapisdk.request;

import com.moming.douapisdk.ApiException;
import com.moming.douapisdk.client.DefaultDouYinClient;
import com.moming.douapisdk.client.DouYinClient;
import com.moming.douapisdk.response.CommentListResponse;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import java.time.LocalDateTime;

import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 * @author lujingpo
 * @date 2021/2/3
 */
@SpringBootTest
@ActiveProfiles(value = "local")
class CommentListRequestTest {

    @Autowired
    private DouYinClient defaultDouYinClient;

    @Test
    void testRequest() throws ApiException {
        CommentListRequest request = new CommentListRequest();
        request.setOrderId("4876008746911882669");
        request.setIsDesc("0");

        CommentListResponse execute = defaultDouYinClient.execute(request);

        assertTrue(true);

    }
}