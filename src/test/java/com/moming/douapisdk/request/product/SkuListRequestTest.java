package com.moming.douapisdk.request.product;

import com.moming.douapisdk.ApiException;
import com.moming.douapisdk.client.DouYinClient;
import com.moming.douapisdk.request.order.GetSettleBillDetailV3Request;
import com.moming.douapisdk.response.order.GetSettleBillDetailV3Response;
import com.moming.douapisdk.response.product.SkuListResponse;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Collections;

import static org.junit.jupiter.api.Assertions.*;

/**
 * @author lujingpo
 * @date 2022/3/22
 */
@SpringBootTest
@ActiveProfiles(value = "local")
class SkuListRequestTest {

    @Autowired
    private DouYinClient douYinClient;

    @Test
    void testInvoke() throws ApiException {
        SkuListRequest skuListRequest = new SkuListRequest();
        skuListRequest.setProductId(3540020826474726212L);

        SkuListResponse execute = douYinClient.execute(skuListRequest);
        assertNotNull(execute);
    }

    @Test
    void testGetSettleBillDetailV3() throws ApiException {
        GetSettleBillDetailV3Request request = new GetSettleBillDetailV3Request();
        // request.setOrderId(Collections.singletonList("4882882833460698823"));
        // request.setSize(100L);
        // LocalDateTime startTime = LocalDateTime.parse("2023-01-18T00:00:00", DateTimeFormatter.ISO_LOCAL_DATE_TIME);
        // LocalDateTime endTime = startTime.plusSeconds(5);
        // request.setStartTime(startTime);
        // request.setEndTime(endTime);
        request.setOrderId(Collections.singletonList("5010350848554467195F00"));
        request.setSize(100L);


        GetSettleBillDetailV3Response execute = douYinClient.execute(request);
        for (GetSettleBillDetailV3Response.SettleBillDetailV3 datum : execute.getData()) {
            String orderId = datum.getOrderId();
            Long aLong = Long.valueOf(orderId);
            System.out.println(aLong);
        }
        assertNotNull(execute);
    }
}