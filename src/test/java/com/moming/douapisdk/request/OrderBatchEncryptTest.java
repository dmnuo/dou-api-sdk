package com.moming.douapisdk.request;

import com.moming.douapisdk.ApiException;
import com.moming.douapisdk.client.DouYinClient;
import com.moming.douapisdk.domain.BatchEncrypt;
import com.moming.douapisdk.response.OrderBatchEncryptResponse;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import java.util.*;

import static org.junit.jupiter.api.Assertions.assertNotNull;

/**
 * 批量加密接口测试
 * @author LangYu
 * @date 2021/7/10
 */


@SpringBootTest
@ActiveProfiles(value = "local")
@ConditionalOnProperty(prefix = "spring.dou-yin-local", name = "is-self-use", havingValue = "true")
public class OrderBatchEncryptTest {

    @Autowired
    private DouYinClient douYinClient;


    private final String accessToken = "edae7c30-8386-443b-88a1-031111596fdd";


    @Test
    void test() throws ApiException {
        BatchEncrypt b = new BatchEncrypt();
        b.setAuthId("123");
        b.setPlainText("15925685912");
        b.setSensitiveType(3);
        b.setIsSupportIndex(true);

        List<BatchEncrypt> list = new ArrayList<BatchEncrypt>();
        list.add(b);
        OrderBatchEncryptRequest request = new OrderBatchEncryptRequest();
        request.setBatchEncryptList(list);
        OrderBatchEncryptResponse execute = douYinClient.execute(request);
        assertNotNull(execute);
    }

}
