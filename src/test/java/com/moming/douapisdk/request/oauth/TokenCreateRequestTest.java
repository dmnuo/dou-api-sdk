package com.moming.douapisdk.request.oauth;

import com.moming.douapisdk.ApiException;
import com.moming.douapisdk.client.DouYinClient;
import com.moming.douapisdk.response.OauthResponse;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import static org.junit.jupiter.api.Assertions.*;

/**
 * @author lujingpo
 * @date 2021/7/7
 */
@SpringBootTest
@ActiveProfiles(value = "local")
class TokenCreateRequestTest {

    @Autowired
    private DouYinClient douYinClient;

    @Test
    void testTokenCreate() throws ApiException {
        TokenCreateRequest request = new TokenCreateRequest();
        request.setShopId("4463798");
        request.setCode("0");
        request.setGrantType(TokenCreateRequest.GrantTypeEnum.AUTHORIZATION_CODE);

        OauthResponse tokenCreateResponse = douYinClient.execute(request);
        assertNotNull(tokenCreateResponse);
        assertEquals(tokenCreateResponse.getShopId(), "4463798");

    }

    @Test
    void testTokenRefresh() throws ApiException {
        TokenRefreshRequest request = new TokenRefreshRequest();
        request.setRefreshToken("9e6a3f9b-91a1-4e37-87a6-09a1dd4c7373");
        OauthResponse execute = douYinClient.execute(request);
        assertNotNull(execute);
        assertEquals(execute.getShopId(), "4463798");
    }
}