package com.moming.douapisdk.request;

import com.moming.douapisdk.ApiException;
import com.moming.douapisdk.client.DouYinClient;
import com.moming.douapisdk.domain.PlainText;
import com.moming.douapisdk.response.OrderBatchSearchIndexResponse;
import com.moming.douapisdk.response.OrderGetSearchIndexResponse;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import java.util.*;

import static org.junit.jupiter.api.Assertions.assertNotNull;

/**
 * 批量获取索引串测试
 * @author LangYu
 * @date 2021/7/10
 */
@SpringBootTest
@ActiveProfiles(value = "local")
@ConditionalOnProperty(prefix = "spring.dou-yin", name = "is-self-use", havingValue = "true")
public class OrderBatchSearchIndexTest {

    @Autowired
    private DouYinClient douYinClient;


    private final String accessToken = "edae7c30-8386-443b-88a1-031111596fdd";

    @Test
    void test() throws ApiException {
        PlainText p = new PlainText();
        p.setEncryptType(3);
        p.setPlainText("13117428564");

        List<PlainText> list = new ArrayList<PlainText>();

        list.add(p);
        OrderBatchSearchIndexRequest request = new OrderBatchSearchIndexRequest();
        request.setPlainTextList(list);
        OrderBatchSearchIndexResponse execute = douYinClient.execute(request);
        assertNotNull(execute);
    }



}
