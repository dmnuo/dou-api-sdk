package com.moming.douapisdk.request.aftersale;

import com.moming.douapisdk.ApiException;
import com.moming.douapisdk.client.DouYinClient;
import com.moming.douapisdk.response.aftersale.AfterSaleDetailResponse;
import org.apache.http.util.Asserts;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import java.io.File;
import java.io.IOException;

import static org.junit.jupiter.api.Assertions.*;

/**
 * @author lujingpo
 * @date 2022/8/23
 */
@SpringBootTest
@ActiveProfiles(value = "local")
class AfterSaleDetailRequestTest {

    @Autowired
    private DouYinClient douYinClient;

    @Test
    void api() throws ApiException {
        AfterSaleDetailRequest request = new AfterSaleDetailRequest();
        request.setAfterSaleId("7090195172318904608");

        AfterSaleDetailResponse execute = douYinClient.execute(request);

        Assertions.assertNotNull(execute.getProcessInfo());

    }
}