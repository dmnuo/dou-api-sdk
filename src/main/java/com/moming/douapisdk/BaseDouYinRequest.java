package com.moming.douapisdk;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;

/**
 * @author tianzong
 * @date 2020/7/21
 */
public abstract class BaseDouYinRequest<T extends BaseDouYinResponse> implements DouYinRequest<T> {
    private final Class<T> clazz;

    public BaseDouYinRequest() {
        ParameterizedType superclass = (ParameterizedType) getClass().getGenericSuperclass();

        Type actualTypeArgument = superclass.getActualTypeArguments()[0];
        clazz = (Class<T>) actualTypeArgument;
    }

    /**
     * 返回的数据中data是否是一个数组
     *
     * <pre>
     *     其他的接口返回的data都是一个对象，可以直接解析到response对象上面，只有
     *     物流公司这个，data直接是一个数组，特殊返回特殊处理
     *     参考链接 <a href="https://op.jinritemai.com/docs/api-docs/16/76">物流调用api</a>
     * </pre>
     * @return boolean
     */
    @Override
    public boolean isArrayResponseData() {
        return false;
    }

    @Override
    public Class<T> getResponseClass() {
        return clazz;
    }
}
