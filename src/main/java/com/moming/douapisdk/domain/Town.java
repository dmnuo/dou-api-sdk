package com.moming.douapisdk.domain;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;

/**
 * 县/区
 *
 * @author lujingpo
 * @since 2023/5/6
 */
@Data
public class Town {

    /**
     * 名称
     * example:"奉贤区"
     */
    @JSONField(name = "name")
    private String name;

    /**
     * 地区ID
     * example:"310120"
     */
    @JSONField(name = "id")
    private String id;

}
