package com.moming.douapisdk.domain;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;

/**
 * 赠品活动信息
 *
 * @author lujingpo
 * @since 2023/5/6
 */
@Data
public class GivenProductActivityInfo {

    /**
     * NORMAL-普通的买赠，AMOUNT-满件赠，PRICE-满元赠
     * example = "NORMAL"
     */
    @JSONField(name = "given_product_activity_type")
    private String givenProductActivityType;

    /**
     * 满几件赠或满几元赠，满元赠场景单位是分
     * example = "200"
     */
    @JSONField(name = "given_product_activity_value")
    private String givenProductActivityValue;

}
