package com.moming.douapisdk.domain;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;

/**
 * sku云仓择仓信息（云仓业务使用，非商品区域仓功能使用）
 *
 * @author lujingpo
 * @since 2023/5/6
 */
@Data
public class SkuYunWarehouseInfoItem {

    /**
     * 预分配云仓信息
     * example = ""
     */
    @JSONField(name = "pre_allocated_yun_warehouse_info")
    private PreAllocatedYunWarehouseInfo preAllocatedYunWarehouseInfo;

    /**
     * 云仓实仓信息
     * example = ""
     */
    @JSONField(name = "real_yun_warehouse_info")
    private RealYunWarehouseInfo realYunWarehouseInfo;

    /**
     * 当前业务阶段 1-未分配 2-已分配未发货 2-已发货
     * example = "2"
     */
    @JSONField(name = "current_business_stage")
    private Long currentBusinessStage;

    /**
     * 该仓对应的skuId
     * example = "123"
     */
    @JSONField(name = "sku_id")
    private String skuId;

    /**
     * sku数量
     * example = "1"
     */
    @JSONField(name = "sku_count")
    private Long skuCount;

}
