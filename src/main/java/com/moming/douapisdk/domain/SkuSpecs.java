package com.moming.douapisdk.domain;

import lombok.Data;

/**
 * 规格信息
 *
 * @author lujingpo
 * @date 2021/6/12
 */
@Data
public class SkuSpecs {

    /**
     * 规格名称
     */
    private String name;
    /**
     * 规格值
     */
    private String value;

}
