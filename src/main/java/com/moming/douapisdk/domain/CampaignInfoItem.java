package com.moming.douapisdk.domain;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;

/**
 * 活动信息
 *
 * 其中活动金额会体现到商品价格中（比如商品原价是100元，参加活动优惠20元，那商品现价就为80元）
 * @author lujingpo
 * @since 2023/5/6
 */
@Data
public class CampaignInfoItem {

    /**
     * 活动ID
     * example = "4781320682406083640"
     */
    @JSONField(name = "campaign_id")
    private Long campaignId;

    /**
     * 活动类型：7-限时特卖 10-定金预售 11-定金预售尾款部分 13-达人专属价 14-限时限量限人 120-拼团 170-赠品 180-金币 190-金币 410-?
     * example = "7"
     */
    @JSONField(name = "campaign_type")
    private Long campaignType;

    /**
     * 成本分摊
     * example = ""
     */
    @JSONField(name = "share_discount_cost")
    private ShareDiscountCost shareDiscountCost;

    /**
     * 活动名称
     * example = "XXX活动"
     */
    @JSONField(name = "campaign_name")
    private String campaignName;

    /**
     * 活动金额（单位：分）
     * example = "200"
     */
    @JSONField(name = "campaign_amount")
    private Long campaignAmount;

    /**
     * 活动子类型：0 店铺活动，1 平台活动
     * example = "1"
     */
    @JSONField(name = "campaign_sub_type")
    private Long campaignSubType;
}
