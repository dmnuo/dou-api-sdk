package com.moming.douapisdk.domain;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;

/**
 * 用户特征标签
 *
 * @author lujingpo
 * @since 2023/5/6
 */
@Data
public class UserTagUiItem {
    /**
     * 标签key
     * 【key: user_profile_buy_frequency = text: 服务优先】
     * 【key: user_profile_buy_frequency = text: 高频购买 】
     * 【key: user_profile_shop_customer_type = text: 店铺新客 】
     * 【key:user_profile_shop_customer_type = text:店铺老客】
     *
     * example:"user_profile_shop_customer_type"
     */
    @JSONField(name = "key")
    private String key;

    /**
     * 标签名称
     * 【key: user_profile_buy_frequency = text: 服务优先】
     * 【key: user_profile_buy_frequency = text: 高频购买 】
     * 【key: user_profile_shop_customer_type = text: 店铺新客 】
     * 【key:user_profile_shop_customer_type = text:店铺老客】
     *
     * example:"店铺老客"
     */
    @JSONField(name = "text")
    private String text;
}
