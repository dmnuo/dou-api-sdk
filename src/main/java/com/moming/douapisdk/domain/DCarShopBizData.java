package com.moming.douapisdk.domain;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;

import java.util.List;

/**
 * 懂车帝购车信息
 *
 * @author lujingpo
 * @since 2023/5/6
 */
@Data
public class DCarShopBizData {

    /**
     * 选择的门店ID
     * example: "<nil>"
     */
    @JSONField(name="poi_id")
    private String poiId;

    /**
     * 选择的门店名称
     * example: "<nil>"
     */
    @JSONField(name="poi_name")
    private String poiName;

    /**
     * 选择的门店地址
     * example: "<nil>"
     */
    @JSONField(name="poi_addr")
    private String poiAddr;

    /**
     * 选择的门店电话
     * example: "<nil>"
     */
    @JSONField(name="poi_tel")
    private String poiTel;

    /**
     * 权益信息
     * example: ""
     */
    @JSONField(name="coupon_right")
    private List<CouponRightItem> couponRight;

    /**
     * 选择的门店所在省
     * example: "<nil>"
     */
    @JSONField(name="poi_pname")
    private String poiPname;

    /**
     * 选择的门店所在市
     * example: "<nil>"
     */
    @JSONField(name="poi_city_name")
    private String poiCityName;

    /**
     * 选择的门店所在区县
     * example: "<nil>"
     */
    @JSONField(name="poi_adname")
    private String poiAdname;
}
