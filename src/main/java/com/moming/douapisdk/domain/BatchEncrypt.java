package com.moming.douapisdk.domain;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;

/**
 * @author LangYu
 * @date 2021/7/10
 */
@Data
public class BatchEncrypt {

    /**
     * 明文
     */
    @JSONField(name = "plain_text")
    private String plainText;

    /**
     * 业务标识，value为抖音订单号
     */
    @JSONField(name = "auth_id")
    private String authId;


    /**
     * 是否支持密文索引
     */
    @JSONField(name = "is_support_index")
    private Boolean isSupportIndex;

    /**
     * 加密类型；
     * 1地址加密
     * 2姓名加密
     * 3电话加密
     */
    @JSONField(name = "sensitive_type")
    private Integer sensitiveType;


}
