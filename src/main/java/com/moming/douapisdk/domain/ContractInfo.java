package com.moming.douapisdk.domain;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;

/**
 * 合约信息
 *
 * @author lujingpo
 * @since 2023/5/6
 */
@Data
public class ContractInfo {

    /**
     * 办理合约的手机号
     * example = "13888888888"
     */
    @JSONField(name = "mobile_no")
    private String mobileNo;

    /**
     * 办理合约的手机号
     * example = "13888888888"
     */
    @JSONField(name = "encrypt_mobile_no")
    private String encryptMobileNo;

}
