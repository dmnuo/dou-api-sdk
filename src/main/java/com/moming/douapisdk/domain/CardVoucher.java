package com.moming.douapisdk.domain;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;

/**
 * 商品卡券基本信息
 *
 * @author lujingpo
 * @since 2023/5/6
 */
@Data
public class CardVoucher {

    /**
     * 自领取之日起有效天数(如果存在优先使用)
     * example = "15"
     */
    @JSONField(name = "valid_days")
    private Long validDays;

    /**
     * 卡券开始时间
     * example = "1641312000"
     */
    @JSONField(name = "valid_start")
    private Long validStart;

    /**
     * 卡券截止时间
     * example = "1646063999"
     */
    @JSONField(name = "valid_end")
    private Long validEnd;

}
