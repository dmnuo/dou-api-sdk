package com.moming.douapisdk.domain;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;

/**
 * 买家账号信息
 *
 * @author lujingpo
 * @since 2023/5/6
 */
@Data
public class AccountInfoItem {

    /**
     * 买家账号名称
     * example = "测试"
     */
    @JSONField(name = "account_name")
    private String accountName;

    /**
     * 买家账号类型;账号类型；枚举值：Mobile ：手机号;Email ：邮箱 ; IdCard ：身份证;  Passport ：护照;  BankCard ：银行卡;  Number ：纯数学;  NumberLetter ：数字字母混合;
     * example = "1"
     */
    @JSONField(name = "account_type")
    private String accountType;

    /**
     * 账号值
     * example = "15678909876"
     */
    @JSONField(name = "account_id")
    private String accountId;

    /**
     * 账号值
     * example = "15678909876"
     */
    @JSONField(name = "encrypt_account_id")
    private String encryptAccountId;

}
