package com.moming.douapisdk.domain;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;

/**
 * 定制商品信息
 *
 * @author lujingpo
 * @since 2023/5/6
 */
@Data
public class SkuCustomizationInfoItem {

    /**
     * 定制详情
     */
    @JSONField(name = "detail")
    private Detail detail;
}
