package com.moming.douapisdk.domain;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;

import java.util.List;

/**
 * 虚拟订单时该参数会返回信息
 *
 * @author lujingpo
 * @since 2023/5/6
 */
@Data
public class AccountList {

    /**
     * 买家账号信息
     * example = ""
     */
    @JSONField(name = "account_info")
    private List<AccountInfoItem> accountInfo;

}
