package com.moming.douapisdk.domain;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;

/**
 * @author lujingpo
 * @since 2023/5/6
 */
@Data
public class UserIdInfo {
    /**
     * 证件号
     * example: "#zuLyd4U4J3p+czzXkwg+ZQ673h7KTcrKOddb5iPGAAE0K3MYJmgXEXof9LDtoScAfMKvdVRqpAL4CEI3SrLwYATIzTF9Qw=="
     */
    @JSONField(name = "id_card_no")
    private String idCardNo;

    /**
     * 证件号
     * example: "#zuLyd4U4J3p+czzXkwg+ZQ673h7KTcrKOddb5iPGAAE0K3MYJmgXEXof9LDtoScAfMKvdVRqpAL4CEI3SrLwYATIzTF9Qw=="
     */
    @JSONField(name = "encrypt_id_card_no")
    private String encryptIdCardNo;

    /**
     * 证件姓名
     * example: "#zuLyd4U4J3p+czzXkwg+ZQ673h7KTcrKOddb5iPGAAE0K3MYJmgXEXof9LDtoScAfMKvdVRqpAL4CEI3SrLwYATIzTF9Qw=="
     */
    @JSONField(name = "id_card_name")
    private String idCardName;

    /**
     * 证件姓名
     * example: "#zuLyd4U4J3p+czzXkwg+ZQ673h7KTcrKOddb5iPGAAE0K3MYJmgXEXof9LDtoScAfMKvdVRqpAL4CEI3SrLwYATIzTF9Qw=="
     */
    @JSONField(name = "encrypt_id_card_name")
    private String encryptIdCardName;

}
