package com.moming.douapisdk.domain;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;

/**
 * 买家收货地址经纬度信息，高德坐标系
 *
 * @author lujingpo
 * @since 2023/5/6
 */
@Data
public class UserCoordinate {

    /**
     * 买家收货地址经度信息，高德坐标系；
     * example:"117.250076"
     */
    @JSONField(name = "user_coordinate_longitude")
    private String userCoordinateLongitude;

    /**
     * 买家收货地址纬度信息，高德坐标系；
     * example:"31.707203"
     */
    @JSONField(name = "user_coordinate_latitude")
    private String userCoordinateLatitude;

}
