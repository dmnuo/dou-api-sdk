package com.moming.douapisdk.domain;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

/**
 * 脱敏信息
 * @author LangYu
 * @date 2021/7/10
 */

@Data
public class BatchEncryptList{
    /**
    *明文
     */
    private String plainText;



    /**
     *业务标识，value为抖音订单
     */
    private String authId;

    /**
     *是否支持密文索引
     */
    private Boolean isSupportIndex;
}
