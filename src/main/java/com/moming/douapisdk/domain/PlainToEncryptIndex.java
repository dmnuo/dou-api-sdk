package com.moming.douapisdk.domain;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;

/**
 * 索引串
 * @author LangYu
 * @date 2021/7/10
 */
@Data
public class PlainToEncryptIndex {

    /**
     *明文
     */
    private String plain;


    /**
     * 索引串
     */
    @JSONField(name="search_index")
    private String searchIndex;
}
