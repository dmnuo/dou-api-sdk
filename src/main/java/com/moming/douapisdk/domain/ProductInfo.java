package com.moming.douapisdk.domain;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;

import java.util.List;

/**
 * @author lujingpo
 * @date 2021/6/12
 */
@Data
public class ProductInfo {


    /**
     * 商品名称
     */
    @JSONField(name = "product_name")
    private String productName;

    /**
     * 商品价格
     *
     */
    private Integer price;

    /**
     * 商家编码
     */
    @JSONField(name = "outer_sku_id")
    private String outerSkuId;

    /**
     * 商品skuId
     */
    @JSONField(name = "sku_id")
    private Long skuId;

    /**
     *
     */
    @JSONField(name = "sku_specs")
    private List<SkuSpecs> skuSpecs;

    /**
     * 商品数量
     */
    @JSONField(name = "product_count")
    private Integer productCount;

    /**
     * 商品ID
     */
    @JSONField(name = "product_id")
    private Long productId;

    /**
     * 商品单ID
     */
    @JSONField(name = "sku_order_id")
    private String skuOrderId;

}
