package com.moming.douapisdk.domain;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;

/**
 * 市
 * @author lujingpo
 * @since 2023/5/6
 */
@Data
public class City {

    /**
     * 名称
     * example:"市辖区"
     */
    @JSONField(name = "name")
    private String name;

    /**
     * 地区ID
     * example:"310000"
     */
    @JSONField(name = "id")
    private String id;
}
