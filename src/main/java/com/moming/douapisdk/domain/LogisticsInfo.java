package com.moming.douapisdk.domain;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;

import java.util.List;

/**
 * 物流信息
 *
 * @author lujingpo
 * @date 2021/6/12
 */
@Data
public class LogisticsInfo {


    /**
     * 物流单号
     */
    @JSONField(name = "tracking_no")
    private String trackingNo;

    /**
     * 物流公司
     * <p/>
     * Example: shunfeng
     */
    private String company;

    /**
     * 发货时间
     * <p/>
     * 秒级时间戳 Example: 1617355413
     */
    @JSONField(name = "ship_time")
    private Long shipTime;

    /**
     * 包裹id
     * <p/>
     * Example: shunfeng_3617355413
     */
    @JSONField(name = "delivery_id")
    private String deliveryId;

    /**
     * 物流公司名称
     * <p/>
     * Example: 顺丰
     */
    @JSONField(name = "company_name")
    private String companyName;

    /**
     *
     */
    @JSONField(name = "product_info")
    private List<ProductInfo> productInfo;

}
