package com.moming.douapisdk.domain;

/**
 * @author lujingpo
 * @date 2021/2/3
 */
public interface IRequestParameterEnum<T> {

    /**
     * 枚举参数必须具有该返回值
     * @return 结果
     */
    T getValue();

}
