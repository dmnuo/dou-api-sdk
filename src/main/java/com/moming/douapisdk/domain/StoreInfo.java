package com.moming.douapisdk.domain;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;

/**
 * 门店信息
 *
 * @author lujingpo
 * @since 2023/5/6
 */
@Data
public class StoreInfo {

    /**
     * 门店id
     * example = "1"
     */
    @JSONField(name = "store_id")
    private String storeId;

    /**
     * 门店名称
     * example = "门店名称"
     */
    @JSONField(name = "store_name")
    private String storeName;

    /**
     * 门店电话
     * example = "13888888888"
     */
    @JSONField(name = "store_tel")
    private String storeTel;

    /**
     * 门店地址
     * example = ""
     */
    @JSONField(name = "store_address")
    private StoreAddress storeAddress;

    /**
     * 扩展字段
     * example = "扩展字段"
     */
    @JSONField(name = "extra")
    private String extra;

}
