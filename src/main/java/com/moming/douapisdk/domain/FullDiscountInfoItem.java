package com.moming.douapisdk.domain;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;

/**
 * 优惠活动信息
 *
 * @author lujingpo
 * @since 2023/5/6
 */
@Data
public class FullDiscountInfoItem {

    /**
     * 活动ID
     * example = "4781320682406083640"
     */
    @JSONField(name = "campaign_id")
    private Long campaignId;

    /**
     * 活动类型：100-店铺满减活动 110-平台满减活动 150-立减活动
     * example = "150"
     */
    @JSONField(name = "campaign_type")
    private Long campaignType;

    /**
     * 成本分摊
     * example = ""
     */
    @JSONField(name = "share_discount_cost")
    private ShareDiscountCost shareDiscountCost;

    /**
     * 活动名称
     * example = "XXX活动"
     */
    @JSONField(name = "campaign_name")
    private String campaignName;

    /**
     * 活动优惠金额（单位：分）
     * example = "200"
     */
    @JSONField(name = "campaign_amount")
    private Long campaignAmount;

    /**
     * 活动子类型：0 店铺活动，1 平台活动
     * example = "1"
     */
    @JSONField(name = "campaign_sub_type")
    private Long campaignSubType;

}
