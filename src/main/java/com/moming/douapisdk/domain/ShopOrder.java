package com.moming.douapisdk.domain;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;

import java.util.List;

/**
 * 店铺订单
 *
 * @author lujingpo
 * @date 2021/6/12
 */
@Data
public class ShopOrder {

    /**
     * 小时达订单的接单状态 0-未接单；1-已接单；2-超时取消，或商家取消
     */
    @JSONField(name = "accept_order_status")
    private Long acceptOrderStatus;

    /**
     * 商品序列号（IMEI码或SN码）
     */
    @JSONField(name = "serial_number_list")
    private List<String> serialNumberList;

    /**
     * 红包优惠金额（单位：分）
     */
    @JSONField(name = "promotion_redpack_amount")
    private Long promotionRedpackAmount;

    /**
     * 平台红包优惠金额（单位：分），属于平台的红包的优惠金额
     * example: 0
     */
    @JSONField(name = "promotion_redpack_platform_amount")
    private Long promotionRedpackPlatformAmount;

    /**
     * 达人红包优惠金额（单位：分），属于达人的红包的优惠金额
     * example: 0
     */
    @JSONField(name = "promotion_redpack_talent_amount")
    private Long promotionRedpackTalentAmount;

    /**
     * 用户证件信息
     * example: 0
     */
    @JSONField(name = "user_id_info")
    private UserIdInfo userIdInfo;

    /**
     * 预约发货时间
     * example: 0
     */
    @JSONField(name = "appointment_ship_time")
    private Long appointmentShipTime;

    /**
     * 懂车帝购车信息
     * example: ""
     */
    @JSONField(name = "d_car_shop_biz_data")
    private DCarShopBizData dCarShopBizData;

    /**
     * 店铺单标签
     * example: ""
     * <a href="https://op.jinritemai.com/docs/guide-docs/1215/3001">订单来源标签说明</a>
     */
    @JSONField(name = "shop_order_tag_ui")
    private List<ShopOrderTagUiItem> shopOrderTagUi;

    /**
     * 总优惠金额（单位：分）
     * total_promotion_amount = promotion_amount + post_promotion_amount
     * example: 0
     */
    @JSONField(name = "total_promotion_amount")
    private Long totalPromotionAmount;

    /**
     * 运费原价（单位：分）
     * post_origin_amount = post_amount + post_promotion_amount
     * example: 500
     */
    @JSONField(name = "post_origin_amount")
    private Long postOriginAmount;

    /**
     * 运费优惠金额（单位：分）
     * example: 0
     */
    @JSONField(name = "post_promotion_amount")
    private Long postPromotionAmount;

    /**
     * 用户特征标签；
     * example: 0
     */
    @JSONField(name = "user_tag_ui")
    private List<UserTagUiItem> userTagUi;

    /**
     * 作者（达人）承担金额（单位：分）
     * 订单参与活动和优惠中作者（达人）承担部分的总金额
     * example: 0
     */
    @JSONField(name = "author_cost_amount")
    private Long authorCostAmount;

    /**
     * 仅平台承担金额（单位：分）
     * 订单参与活动和优惠中平台承担部分的总金额
     *
     * example: 0
     */
    @JSONField(name = "only_platform_cost_amount")
    private Long onlyPlatformCostAmount;

    /**
     * 履约时效信息(json串)
     *
     * example: {identifier:S#2023-04-26}
     */
    @JSONField(name = "promise_info")
    private String promiseInfo;

    /**
     * 收件人姓名（脱敏后）
     * example: 生***
     */
    @JSONField(name = "mask_post_receiver")
    private String maskPostReceiver;

    /**
     * 收件人电话（脱敏后）
     * example: 1********20
     */
    @JSONField(name = "mask_post_tel")
    private String maskPostTel;

    /**
     * 收件人地址（脱敏后）
     * example: "")
     */
    @JSONField(name = "mask_post_addr")
    private MaskPostAddr maskPostAddr;

    /**
     * 买家收货地址经纬度信息，高德坐标系；
     * example: ""
     */
    @JSONField(name = "user_coordinate")
    private UserCoordinate userCoordinate;

    /**
     * 预计最早送达时间，Unix时间戳：秒；当early_arrival=false时使用，仅小时达业务返回；
     * example: "0"
     */
    @JSONField(name = "earliest_receipt_time")
    private Long earliestReceiptTime;

    /**
     * 预计最晚送达时间，Unix时间戳：秒；当early_arrival=false时使用，仅小时达业务返回；
     * example: "0"
     */
    @JSONField(name = "latest_receipt_time")
    private Long latestReceiptTime;

    /**
     * 是否尽快送达，true-是（配合target_arrival_time字段使用），false-否（配合earliest_receipt_time和latest_receipt_time使用），仅小时达业务返回；
     * example: "false"
     */
    @JSONField(name = "early_arrival")
    private Boolean earlyArrival;

    /**
     * 尽快送达的时间点，Unix时间戳：秒；当early_arrival=true时使用，仅小时达业务返回；
     * example: "0"
     */
    @JSONField(name = "target_arrival_time")
    private Long targetArrivalTime;

    /**
     * 打包费，单位：分；
     * example: "0"
     */
    @JSONField(name = "packing_amount")
    private Long packingAmount;

    /**
     * 门店流水号；仅小时达业务返回
     * example: "135334"
     */
    @JSONField(name = "supermarket_order_serial_no")
    private String supermarketOrderSerialNo;

    /**
     * 税费(子单税费之和)
     * example: "0"
     */
    @JSONField(name = "tax_amount")
    private Long taxAmount;

    /**
     * 地址标签列表
     * example: ""
     */
    @JSONField(name = "address_tag_ui")
    private List<AddressTagUiItem> addressTagUi;

    /**
     * 脱敏下单人手机号（鲜花订单）
     * example: "XXX"
     */
    @JSONField(name = "mask_pay_tel")
    private String maskPayTel;

    /**
     * 下单人手机号（鲜花订单）
     * example: "134"
     */
    @JSONField(name = "pay_tel")
    private String payTel;

    /**
     * 下单人手机号（鲜花订单）
     * example: "134"
     */
    @JSONField(name = "encrypt_pay_tel")
    private String encryptPayTel;

    /**
     * 贺卡文字(鲜花订单)
     * example: "贺卡"
     */
    @JSONField(name = "greet_words")
    private String greetWords;


    /**
     * 店铺ID
     */
    @JSONField(name = "app_id")
    private Integer appId;

    /**
     * app渠道
     * <p></p>
     * Example:
     * <ul>
     * <li>0 站外</li>
     * <li>1 火山</li>
     * <li>2 抖音</li>
     * <li>3 头条</li>
     * <li>4 西瓜</li>
     * <li>5 微信</li>
     * <li>6 值点app</li>
     * <li>7 头条lite</li>
     * <li>8 懂车帝</li>
     * <li>9 皮皮虾</li>
     * <li>11 抖音极速版</li>
     * <li>12 TikTok</li>
     * <li>13 musically</li>
     * <li>14 穿山甲</li>
     * <li>15 火山极速版</li>
     * <li>16 服务市场</li>
     * </ul>
     */
    @JSONField(name = "b_type")
    private Integer bType;
    /**
     * 下单端描述
     */
    @JSONField(name = "b_type_desc")
    private String bTypeDesc;

    /**
     * 业务来源
     * Example:
     * <blockquote>
     * <ul>
     * <li>1-鲁班</li>
     * <li>2-小店</li>
     * <li>3-好好学习等</li>
     * </ul>
     * </blockquote>
     */
    private Integer biz;
    /**
     * 业务来源描述
     */
    @JSONField(name = "biz_desc")
    private String bizDesc;
    /**
     * 买家留言
     */
    @JSONField(name = "buyer_words")
    private String buyerWords;
    /**
     * 取消原因
     */
    @JSONField(name = "cancel_reason")
    private String cancelReason;
    /**
     * 支付流水号
     */
    @JSONField(name = "channel_payment_no")
    private String channelPaymentNo;
    /**
     * 下单时间
     * <p/>
     * 秒级别时间戳<br/>
     * Example: 1622977290
     */
    @JSONField(name = "create_time")
    private Long createTime;
    /**
     * 密文出参
     * <p/>
     * 参考文档: <a href="https://bytedance.feishu.cn/docs/doccnJNKML3jOzFgjJitzXy61lh#">加解密指南</a>
     */
    @JSONField(name = "encrypt_post_receiver")
    private String encryptPostReceiver;
    /**
     * 收件人电话
     *
     */
    @JSONField(name = "encrypt_post_tel")
    private String encryptPostTel;
    /**
     * 预计发货时间
     * <p/>
     * 秒级时间戳 <br/>
     * Example: 1622977290
     */
    @JSONField(name = "exp_ship_time")
    private Long expShipTime;
    /**
     * 订单完成时间
     * <p/>
     * 秒级时间戳 <br/>
     * Example: 1622977290
     */
    @JSONField(name = "finish_time")
    private Long finishTime;
    /**
     * 物流信息
     */
    @JSONField(name = "logistics_info")
    private List<LogisticsInfo> logisticsInfo;
    /**
     * 主流程状态
     * <p/>
     * Example: 103
     * <br/>
     * 参考文档：<a href="https://op.jinritemai.com/docs/guide-docs/33/136">订单状态机</a>
     */
    @JSONField(name = "main_status")
    private Integer mainStatus;
    /**
     * 主流程状态描述
     * <p/>
     * Example: 部分支付
     */
    @JSONField(name = "main_status_desc")
    private String mainStatusDesc;
    /**
     * 改价金额变化量
     * <p/>
     * Example: -10
     */
    @JSONField(name = "modify_amount")
    private Integer modifyAmount;
    /**
     * 改价运费金额变化量
     * <p/>
     * Example: -1
     */
    @JSONField(name = "modify_post_amount")
    private Integer modifyPostAmount;
    /**
     * 抖音小程序id
     */
    @JSONField(name = "open_id")
    private String openId;
    /**
     * 订单金额（分）
     */
    @JSONField(name = "order_amount")
    private Integer orderAmount;
    /**
     * 订单过期时间
     * <p/>
     * 单位秒 <br/>
     * Example: 1800
     *
     */
    @JSONField(name = "order_expire_time")
    private Long orderExpireTime;
    /**
     * 店铺订单号
     */
    @JSONField(name = "order_id")
    private String orderId;
    /**
     * 订单层级
     *
     */
    @JSONField(name = "order_level")
    private Integer orderLevel;
    /**
     * 定金预售阶段单
     */
    @JSONField(name = "order_phase_list")
    private List<OrderPhase> orderPhaseList;
    /**
     * 订单状态
     * <p/>
     * 参考文档：<a href="https://op.jinritemai.com/docs/guide-docs/33/136">订单状态机</a>
     */
    @JSONField(name = "order_status")
    private Integer orderStatus;
    /**
     * 订单状态描述
     */
    @JSONField(name = "order_status_desc")
    private String orderStatusDesc;
    /**
     * 订类型
     * <p/>
     * <blockquote>
     * <ul>
     * <li>0-普通订单</li>
     * <li>2-虚拟订单</li>
     * <li>4-平台券码</li>
     * <li>5-商家券码</li>
     * </ul>
     * </blockquote>
     *
     */
    @JSONField(name = "order_type")
    private Integer orderType;
    /**
     * 订单类型描述
     */
    @JSONField(name = "order_type_desc")
    private String orderTypeDesc;
    /**
     * 支付金额（分）
     */
    @JSONField(name = "pay_amount")
    private Integer payAmount;
    /**
     * 支付时间
     * <p/>
     * 秒级时间戳 <br/>
     */
    @JSONField(name = "pay_time")
    private Long payTime;
    /**
     * 支付类型
     * <p/>
     * <ul>
     * <li>0-货到付款</li>
     * <li>1-微信</li>
     * <li>2-支付宝</li>
     * </ul>
     */
    @JSONField(name = "pay_type")
    private Integer payType;
    /**
     * 平台优惠金额平台承担部分
     * <p/>
     * 单位：分
     */
    @JSONField(name = "platform_cost_amount")
    private Integer platformCostAmount;
    /**
     * 收件人地址
     */
    @JSONField(name = "post_addr")
    private PostAddr postAddr;
    /**
     * 快递费（分）
     */
    @JSONField(name = "post_amount")
    private Integer postAmount;
    /**
     * 运费险金额
     */
    @JSONField(name = "post_insurance_amount")
    private Integer postInsuranceAmount;
    /**
     * 收件人姓名
     * @deprecated 数据加密完成之后，需要使用 {@link #encryptPostReceiver}
     */
    @JSONField(name = "post_receiver")
    private String postReceiver;
    /**
     * 收件人手机号
     * @deprecated 数据加密完成之后，需要使用 {@link #encryptPostTel}
     */
    @JSONField(name = "post_tel")
    private String postTel;
    /**
     * 单优惠总金额
     * <p/>
     * 计算方法：
     * <blockquote>单优惠总金额 = 店铺优惠金额+ 平台优惠金额+ 达人优惠金额+ 支付优惠金额</blockquote>
     */
    @JSONField(name = "promotion_amount")
    private Integer promotionAmount;
    /**
     * 支付优惠金额
     */
    @JSONField(name = "promotion_pay_amount")
    private Integer promotionPayAmount;
    /**
     * 平台优惠金额
     */
    @JSONField(name = "promotion_platform_amount")
    private Integer promotionPlatformAmount;
    /**
     * 店铺优惠金额
     */
    @JSONField(name = "promotion_shop_amount")
    private Integer promotionShopAmount;
    /**
     * 达人优惠金额
     */
    @JSONField(name = "promotion_talent_amount")
    private Integer promotionTalentAmount;
    /**
     * 卖家订单标记
     * <p/>
     * 小旗子star取值0～5，分别表示
     * <ul>
     * <li>0-灰</li>
     * <li>1-紫</li>
     * <li>2-青</li>
     * <li>3-绿</li>
     * <li>4-橙</li>
     * <li>5-红</li>
     * </ul>
     */
    @JSONField(name = "seller_remark_stars")
    private Integer sellerRemarkStars;
    /**
     * 商家备注
     */
    @JSONField(name = "seller_words")
    private String sellerWords;
    /**
     * 发货时间
     * <p/>
     * 秒级时间戳
     */
    @JSONField(name = "ship_time")
    private Long shipTime;
    /**
     * 平台优惠金额卖家承担部分
     * <p/>
     * 单位：分
     */
    @JSONField(name = "shop_cost_amount")
    private Integer shopCostAmount;
    /**
     * 店铺ID
     */
    @JSONField(name = "shop_id")
    private Long shopId;
    /**
     * 商户名称
     */
    @JSONField(name = "shop_name")
    private String shopName;
    /**
     * 商品单信息
     */
    @JSONField(name = "sku_order_list")
    private List<SkuOrder> skuOrderList;
    /**
     * 下单场景
     * <p/>
     * 可选值：
     * <ul>
     * <li>0 未知</li>
     * <li>1 app</li>
     * <li>2 小程序</li>
     * <li>3 H5</li>
     * </ul>
     */
    @JSONField(name = "sub_b_type")
    private Integer subBType;
    /**
     * 下单场景描述
     */
    @JSONField(name = "sub_b_type_desc")
    private String subBTypeDesc;

    /**
     * 交易类型
     * <p/>
     * 可选值：
     * <ul>
     * <li>1 拼团订单</li>
     * <li>2 定金预售</li>
     * </ul>
     */
    @JSONField(name = "trade_type")
    private Integer tradeType;
    /**
     * 交易类型描述
     */
    @JSONField(name = "trade_type_desc")
    private String tradeTypeDesc;
    /**
     * 订单更新时间
     * <p/>
     * 秒级时间戳
     */
    @JSONField(name = "update_time")
    private Long updateTime;

    /**
     * 优惠信息
     */
    @JSONField(name = "promotion_detail")
    private PromotionDetail promotionDetail;

    /**
     * 加密用户ID串
     */
    @JSONField(name = "doudian_open_id")
    private String doudianOpenId;


}
