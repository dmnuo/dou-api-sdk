package com.moming.douapisdk.domain;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;

/**
 * 关联订单
 *
 * @author lujingpo
 * @since 2023/5/6
 */
@Data
public class RelationOrder {

    /**
     * 核销券码
     * example = "xxxx"
     */
    @JSONField(name = "write_off_no")
    private String writeOffNo;

    /**
     * 关联店铺单订单id
     * example = "1234567"
     */
    @JSONField(name = "relation_order_id")
    private String relationOrderId;

}
