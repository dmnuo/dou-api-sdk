package com.moming.douapisdk.domain;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;

/**
 * 优惠券信息
 * @author lujingpo
 * @since 2023/5/6
 */
@Data
public class CouponInfoItem {

    /**
     * 订单优惠券ID
     * example = "4781320682406"
     */
    @JSONField(name = "coupon_id")
    private Long couponId;

    /**
     * 券类型；21-商家折扣券；22-商家直减券；23-商家满减券 ；41-单品折扣券；42-单品直减券；43 单品满减券；
     * example = "1"
     */
    @JSONField(name = "coupon_type")
    private Long couponType;

    /**
     * 券批次ID
     * example = "43543523532"
     */
    @JSONField(name = "coupon_meta_id")
    private String couponMetaId;

    /**
     * 券优惠金额（单位：分）
     * example = "100"
     */
    @JSONField(name = "coupon_amount")
    private Long couponAmount;

    /**
     * 券名称
     * example = "优惠券"
     */
    @JSONField(name = "coupon_name")
    private String couponName;

    /**
     * 成本分摊
     * example = ""
     */
    @JSONField(name = "share_discount_cost")
    private ShareDiscountCost shareDiscountCost;

}
