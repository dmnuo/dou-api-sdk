package com.moming.douapisdk.domain;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;

/**
 * 收件人地址（脱敏后）
 *
 * @author lujingpo
 * @since 2023/5/6
 */
@Data
public class MaskPostAddr {

    /**
     * 省
     * example:""
     */
    @JSONField(name = "province")
    private Province province;

    /**
     * 市
     * example:""
     */
    @JSONField(name = "city")
    private City city;

    /**
     * 县/区
     * example:""
     */
    @JSONField(name = "town")
    private Town town;

    /**
     * 街道
     * example:""
     */
    @JSONField(name = "street")
    private Street street;

    /**
     * 详细信息（脱敏后）
     * example:"***"
     */
    @JSONField(name = "detail")
    private String detail;
}
