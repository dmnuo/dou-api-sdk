package com.moming.douapisdk.domain;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;

import java.util.List;

/**
 * 达人优惠信息
 *
 * @author lujingpo
 * @since 2023/5/6
 */
@Data
public class KolDiscountDetail {

    /**
     * 优惠总金额（单位：分）
     * example = "0"
     */
    @JSONField(name = "total_amount")
    private Long totalAmount;

    /**
     * 券优惠金额（单位：分）
     * example = "0"
     */
    @JSONField(name = "coupon_amount")
    private Long couponAmount;

    /**
     * 活动优惠金额（单位：分）
     * example = "0"
     */
    @JSONField(name = "full_discount_amount")
    private Long fullDiscountAmount;

    /**
     * 优惠券信息
     * example = ""
     */
    @JSONField(name = "coupon_info")
    private List<CouponInfoItem> couponInfo;

    /**
     * 优惠活动信息
     * example = ""
     */
    @JSONField(name = "full_discount_info")
    private List<FullDiscountInfoItem> fullDiscountInfo;

    /**
     * 红包金额（单位：分）
     * example = "0"
     */
    @JSONField(name = "redpack_amount")
    private Long redpackAmount;

    /**
     * 红包信息
     * example = ""
     */
    @JSONField(name = "redpack_info")
    private List<RedpackInfoItem> redpackInfo;

}
