package com.moming.douapisdk.domain;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;

/**
 * 预分配云仓信息
 *
 * @author lujingpo
 * @since 2023/5/6
 */
@Data
public class PreAllocatedYunWarehouseInfo {

    /**
     * 云仓code
     * example = "001"
     */
    @JSONField(name = "yun_warehouse_code")
    private String yunWarehouseCode;

    /**
     * 云仓名称
     * example = "仓名称"
     */
    @JSONField(name = "yun_warehouse_name")
    private String yunWarehouseName;

}
