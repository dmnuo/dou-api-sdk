package com.moming.douapisdk.domain;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;

/**
 * 优惠信息
 *
 * @author lujingpo
 * @since 2023/5/6
 */
@Data
public class PromotionDetail_4_4 {

    /**
     * 店铺优惠信息
     * example = ""
     */
    @JSONField(name = "shop_discount_detail")
    private ShopDiscountDetail shopDiscountDetail;

    /**
     * 平台优惠信息
     * example = ""
     */
    @JSONField(name = "platform_discount_detail")
    private PlatformDiscountDetail_5_5 platformDiscountDetail;

    /**
     * 达人优惠信息
     * example = ""
     */
    @JSONField(name = "kol_discount_detail")
    private KolDiscountDetail kolDiscountDetail;

}
