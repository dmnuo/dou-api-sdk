package com.moming.douapisdk.domain;

import com.alibaba.fastjson.annotation.JSONField;

import java.util.List;

/**
 *
 * @author lujingpo
 * @since 2023/5/6
 */
public class Detail {

    /**
     * 定制图片信息
     * example = ""
     */
    @JSONField(name = "pic")
    private List<PicItem> pic;

    /**
     * 定制文案信息
     * example = ""
     */
    @JSONField(name = "text")
    private List<TextItem> text;

    /**
     * 额外信息
     * example = "{key:value}"
     */
    @JSONField(name = "extra")
    private String extra;

}
