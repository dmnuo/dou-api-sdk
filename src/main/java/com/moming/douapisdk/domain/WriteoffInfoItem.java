package com.moming.douapisdk.domain;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;

/**
 * 核销信息
 *
 * @author lujingpo
 * @since 2023/5/6
 */
@Data
public class WriteoffInfoItem {

    /**
     * 核销券码
     * example = "xxxx")
     */
    @JSONField(name = "writeoff_no")
    private String writeoffNo;

    /**
     * 核销有效开始时间
     * example = "1617355413")
     */
    @JSONField(name = "writeoff_start_time")
    private Long writeoffStartTime;

    /**
     * 核销过期时间
     * example = "1617355413")
     */
    @JSONField(name = "writeoff_expire_time")
    private Long writeoffExpireTime;

    /**
     * 核销状态
     * example = "3")
     */
    @JSONField(name = "writeoff_status")
    private Long writeoffStatus;

    /**
     * 核销状态文案
     * example = "已使用")
     */
    @JSONField(name = "writeoff_status_desc")
    private String writeoffStatusDesc;

    /**
     * 核销店铺订单id
     * example = "1234567")
     */
    @JSONField(name = "verify_order_id")
    private String verifyOrderId;

    /**
     * 脱敏核销券码
     * example = "Te****1168")
     */
    @JSONField(name = "writeoff_no_mask")
    private String writeoffNoMask;

    /**
     * 已核销次数
     * example = "1")
     */
    @JSONField(name = "writtenoff_count")
    private Long writtenoffCount;

    /**
     * 总核销次数
     * example = "2")
     */
    @JSONField(name = "writeoff_total_count")
    private Long writeoffTotalCount;

}
