package com.moming.douapisdk.domain;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;

/**
 * 红包信息
 *
 * @author lujingpo
 * @since 2023/5/6
 */
@Data
public class RedpackInfoItem {

    /**
     * 红包汇总ID
     * example = "675657456546547"
     */
    @JSONField(name = "redpack_trans_id")
    private String redpackTransId;

    /**
     * 红包金额（单位：分）
     * example = "100"
     */
    @JSONField(name = "redpack_amount")
    private Long redpackAmount;

    /**
     * 成本分摊
     * example = ""
     */
    @JSONField(name = "share_discount_cost")
    private ShareDiscountCost shareDiscountCost;

}
