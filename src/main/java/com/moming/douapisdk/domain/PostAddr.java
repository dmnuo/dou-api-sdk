package com.moming.douapisdk.domain;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * 地址信息
 *
 * @author tianzong
 * @date 2020/7/23
 */
@Data
public class PostAddr {

    /**
     * 城市
     */
    private City city;

    /**
     * 详细地址
     * @deprecated since 1.2.0 数据加密之后，该字段不再返回数据
     */
    private String detail;

    /**
     *  加密的详细地址
     */
    @JSONField(name = "encrypt_detail")
    private String encryptDetail;

    /**
     * 省份
     */
    private Province province;

    /**
     * 城镇
     */
    private Town town;

    /**
     * 街道
     */
    private Street street;

    @Data
    public static class City {
        /**
         * id : 150100
         * name : 呼和浩特市
         */

        private String id;
        private String name;


    }

    @Setter
    @Getter
    @ToString
    public static class Province {
        /**
         * id : 150000
         * name : 内蒙古自治区
         */

        private String id;
        private String name;


    }

    @Setter
    @Getter
    @ToString
    public static class Town {
        /**
         * id : 150101
         * name : 市辖区
         */

        private String id;
        private String name;


    }


    @Setter
    @Getter
    @ToString
    public static class Street {

        private String id;
        private String name;
    }
}
