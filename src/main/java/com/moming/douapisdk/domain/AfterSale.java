package com.moming.douapisdk.domain;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @author lujingpo
 * @date 2021/2/1
 */
@Data
public class AfterSale implements Serializable {
    private static final long serialVersionUID = -1628730831208323973L;
    /**
     * 订单号
     */
    @JSONField(name = "order_id")
    private Long orderId;

    /**
     * 售后单号
     */
    @JSONField(name = "aftersale_id")
    private Long afterSaleId;

    /**
     * 申请售后时间
     */
    @JSONField(name = "apply_time")
    private String applyTime;

    /**
     * 售后类型
     *
     * 0 售后退货退款
     * 1 售后退款
     * 2 售前退款
     * 3 换货
     */
    @JSONField(name = "aftersale_type")
    private Integer afterSaleType;

    /**
     * 超时自动流转截止时间
     */
    @JSONField(name = "status_deadline")
    private String statusDeadline;

    /**
     * 自动流转类型
     */
    @JSONField(name = "deadline_type")
    private Integer deadlineType;

    /**
     * 售后流程描述：仅退款，退货退款等
     */
    @JSONField(name = "aftersale_process_desc")
    private String afterSaleProcessDesc;

    /**
     * 售后状态描述：待卖家发货，待买家退货等
     */
    @JSONField(name = "aftersale_status_desc")
    private String afterSaleStatusDesc;

    /**
     * 退货物流状态描述：未发货，已发货等
     */
    @JSONField(name = "return_status_desc")
    private String returnStatusDesc;

    /**
     * 售后原因描述：买错了等
     */
    @JSONField(name = "reason_desc")
    private String reasonDesc;

    /**
     * 全额部分退款
     * 0 全额退款
     * 1 部分退款
     */
    @JSONField(name = "part_type")
    private Integer partType;

    /**
     * 店铺订单id（主订单id）
     */
    private Long pid;

    /**
     * 退款类型描述：线下退款等
     */
    @JSONField(name = "aftersale_refund_type_desc")
    private String afterSaleRefundTypeDesc;

    /**
     * 售后退款类型
     *
     * 0 原路退回
     * 1 货到付款线下退款
     * 2 备用金退款
     * 3 保证金退款
     * 4 无需退款
     */
    @JSONField(name = "aftersale_refund_type")
    private Integer aftersaleRefundType;

    /**
     * 退款状态
     *
     * 0 无需退款
     * 1 待退款
     * 2 退款中
     * 3 退款成功
     * 4 退款失败
     */
    @JSONField(name = "refund_status")
    private Integer refundStatus;

    /**
     * 售后状态
     *
     * 0 售后初始
     * 6 售后申请
     * 27 拒绝售后申请
     * 12 售后成功
     * 28 售后失败
     * 7 售后退货中
     * 11 售后已发货
     * 29 售后退货拒绝
     * 13 售后换货商家发货
     * 14 售后换货用户收货
     */
    @JSONField(name = "aftersale_status")
    private Integer afterSaleStatus;

    /**
     * 买家收件人名
     */
    @JSONField(name = "post_receiver")
    private String postReceiver;

    /**
     * 仲裁状态
     *
     * 0 无仲裁记录
     * 1 仲裁中
     * 2 客服同意
     * 3 客服拒绝
     */
    @JSONField(name = "arbitrate_status")
    private Integer arbitrateStatus;

    /**
     * 退款类型
     * 0: Origin, 原路退回
     * 1: Offline, 线下退款
     * 2: Impurest,预付款退款
     * 3: Pledge,保证金退款
     * 4: None,无需退款
     * 5: RefundTypeAll, 所有
     */
    @JSONField(name = "refund_type")
    private Integer refundType;

    /**
     * 剩余的催发货短信次数
     */
    @JSONField(name = "urge_sms_cnt")
    private Integer urgeSmsCnt;

    /**
     * 售后申请的子订单信息
     */
    @JSONField(name = "aftersale_items")
    private List<AfterSaleItems> afterSaleItems;

    /**
     * 售后申请历史记录
     */
    @JSONField(name = "aftersale_record_items")
    private List<AfterSaleRecordItems> afterSaleRecordItems;


    /**
     * 售后申请的子订单信息
     */
    @Data
    public static class AfterSaleItems {

        /**
         * 商品名称
         */
        @JSONField(name = "product_name")
        private String productName;

        /**
         * 商品id
         */
        @JSONField(name = "product_id")
        private Long productId;

        /**
         * 商品图片
         */
        @JSONField(name = "product_img")
        private String productImg;

        /**
         * 商品数量
         */
        private Integer num;

        /**
         * 支付金额 单位分
         */
        @JSONField(name = "pay_amount")
        private Integer payAmount;

        /**
         * 邮费 单位分
         */
        @JSONField(name = "post_amount")
        private Integer postAmount;

        /**
         * 退款金额 单位分
         */
        @JSONField(name = "refund_amount")
        private Integer refundAmount;

        /**
         * 退货运费 单位分
         */
        @JSONField(name = "refund_post_amount")
        private Integer refundPostAmount;

        /**
         * 售后标签：七天无理由，极速退等
         */
        @JSONField(name = "aftersale_service")
        private List<String> afterSaleService;

        /**
         * 商品规格
         */
        @JSONField(name = "sku_spec")
        private List<SkuSpec> skuSpec;

        /**
         * 订单id
         */
        @JSONField(name = "order_id")
        private Long orderId;

        /**
         * 创建时间
         */
        @JSONField(name = "create_time")
        private String createTime;

        /**
         * 全额部分退款
         *
         * 0 全额退款
         * 1 部分退款
         */
        @JSONField(name = "part_type")
        private Integer partType;
    }

    /**
     * 商品规格
     */
    @Data
    public static class SkuSpec {

        private String name;

        private String value;
    }

    /**
     * 售后申请历史记录
     */
    @Data
    public static class AfterSaleRecordItems {

        /**
         * 售后单号
         */
        @JSONField(name = "aftersale_id")
        private Long afterSaleId;

        /**
         * 订单号
         */
        @JSONField(name = "order_id")
        private Long orderId;

        /**
         * 操作角色，system：系统、
         * service：平台客服、
         * user：用户、
         * shop：商家
         */
        private String role;

        /**
         * 操作类型
         */
        @JSONField(name = "op_type")
        private String opType;

        /**
         * 操作人名称
         */
        @JSONField(name = "op_name")
        private String opName;

        /**
         * 操作结果，1：同意
         * 2:拒绝
         * 3:立即退款
         */
        private Integer conclusion;

        /**
         * 操作时间
         */
        @JSONField(name = "op_time")
        private String opTime;

        /**
         * 操作意见 原因
         */
        private String comment;

        /**
         * 凭证
         */
        private List<String> evidence;

        /**
         * 说明/备注
         */
        private String remark;

        /**
         * 收到货物说明，0代表未收到货，1代表已收到货
         */
        @JSONField(name = "got_pkg")
        private Integer gotPkg;

        /**
         * 物流单号
         */
        @JSONField(name = "logistics_code")
        private String logisticsCode;

        /**
         * 物流公司编码
         */
        @JSONField(name = "company_code")
        private String companyCode;

        /**
         * 物流公司名称
         */
        @JSONField(name = "company_name")
        private String companyName;

        /**
         * 退款类型
         * 0: Origin, 原路退回
         * 1: Offline, 线下退款
         * 2: Impurest,预付款退款
         * 3: Pledge,保证金退款
         * 4: None,无需退款
         * 5: RefundTypeAll, 所有
         */
        @JSONField(name = "refund_type")
        private Integer refundType;
    }
}
