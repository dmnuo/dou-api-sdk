package com.moming.douapisdk.domain;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;

/**
 * 街道
 * @author lujingpo
 * @since 2023/5/6
 */
@Data
public class Street {

    /**
     * 名称
     * example:"金海街道"
     */
    @JSONField(name = "name")
    private String name;

    /**
     * 地区ID
     * example:"310120001"
     */
    @JSONField(name = "id")
    private String id;

}
