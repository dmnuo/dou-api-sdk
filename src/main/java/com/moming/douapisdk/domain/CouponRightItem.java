package com.moming.douapisdk.domain;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;

/**
 * 权益信息
 * @author lujingpo
 * @since 2023/5/6
 */
@Data
public class CouponRightItem {

    /**
     * 权益类型
     * example:"1"
     */
    @JSONField(name = "right_type")
    private Long rightType;

    /**
     * 权益名称
     * example:"现金抵扣"
     */
    @JSONField(name = "right_name")
    private String rightName;

    /**
     * 权益面值
     * example:"1"
     */
    @JSONField(name = "quota")
    private Long quota;

}
