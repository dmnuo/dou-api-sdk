package com.moming.douapisdk.domain;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;

/**
 * 地址标签列表
 *
 * @author lujingpo
 * @since 2023/5/6
 */
@Data
public class AddressTagUiItem {


    /**
     * 双地址标签 key
     * example:"double_address"
     */
    @JSONField(name = "key")
    private String key;

    /**
     * 双地址标签
     * example:"双地址"
     */
    @JSONField(name = "text")
    private String text;

    /**
     * 双地址标签hover提示
     * example:"用户选择的省、市、区/县、街道与填写的详细地址层级不一致，可能为双地址"
     */
    @JSONField(name = "hover_text")
    private String hoverText;

}
