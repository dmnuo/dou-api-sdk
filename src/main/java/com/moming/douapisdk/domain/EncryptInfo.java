package com.moming.douapisdk.domain;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;

/**
 * 加密后的数据
 * @author LangYu
 * @date 2021/7/10
 */
@Data
public class EncryptInfo {
    /**
     * 鉴权ID
     */
    @JSONField(name = "auth_id")
    private String authId;

    /**
     * 密文
     */
    @JSONField(name = "cipher_text")
    private String cipherText;

    /**
     * 明文
     */
    @JSONField(name = "decrypt_text")
    private String decryptText;

    /**
     * 错误码
     */
    @JSONField(name = "err_no")
    private Integer errNo;

    /**
     * 错误描述
     */
    @JSONField(name = "err_msg")
    private String errMsg;

}
