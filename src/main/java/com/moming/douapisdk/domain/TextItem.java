package com.moming.douapisdk.domain;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;

/**
 * 定制文案信息
 * @author lujingpo
 * @since 2023/5/6
 */
@Data
public class TextItem {

    /**
     * id
     * example = "1"
     */
    @JSONField(name = "id")
    private Long id;

    /**
     * key
     * example = "order_id"
     */
    @JSONField(name = "key")
    private String key;

    /**
     * content
     * example = "1234567"
     */
    @JSONField(name = "content")
    private String content;

}
