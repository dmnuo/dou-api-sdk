package com.moming.douapisdk.domain;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;

/**
 * 省
 *
 * @author lujingpo
 * @since 2023/5/6
 */
@Data
public class Province {

    /**
     * 名称
     * example:"上海市"
     */
    @JSONField(name = "name")
    private String name;

    /**
     * 地区ID
     * example:"31"
     */
    @JSONField(name = "id")
    private String id;

}
