package com.moming.douapisdk.domain;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;

/**
 * 质检信息
 *
 * @author lujingpo
 * @since 2023/5/6
 */
@Data
public class QualityCheckInfo {

    /**
     * 质检结果  WATI_CHECT:待质检, CHECK_PASS:质检通过, CHECK_FAIL:质检不通过
     * example = "CHECK_FAIL"
     */
    @JSONField(name = "check_result_code")
    private String checkResultCode;

    /**
     * 质检异常信息描述文案
     * example = "贵金属不符"
     */
    @JSONField(name = "check_fail_msg")
    private String checkFailMsg;

    /**
     * 重新送检截止时间
     * example = "1664350911"
     */
    @JSONField(name = "resend_check_time")
    private Long resendCheckTime;

}
