package com.moming.douapisdk.domain;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;

/**
 * 组套商品子商品列表
 *
 * @author lujingpo
 * @since 2023/5/6
 */
@Data
public class BundleSkuInfoItem {

    /**
     * 子商品图片
     * example = "https://tosv.boe.byted.org/obj/temai/ffef8099084aeb514b028ec8357d9b4awww640-640"
     */
    @JSONField(name = "picture_url")
    private String pictureUrl;

    /**
     * 子商品在抖店的ID
     * example = "3520562294461467753"
     */
    @JSONField(name = "product_id")
    private String productId;

    /**
     * 子商品名称
     * example = "测试非卖品创建商品"
     */
    @JSONField(name = "product_name")
    private String productName;

    /**
     * 子商品在抖店的SkuId
     * example = "1719024557525047"
     */
    @JSONField(name = "sku_id")
    private String skuId;

    /**
     * 子商品数量
     * example = "1"
     */
    @JSONField(name = "item_num")
    private Long itemNum;

    /**
     * 组套子商品外部编码
     * example = "M8-1234-101"
     */
    @JSONField(name = "code")
    private String code;

}
