package com.moming.douapisdk.domain;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;

import java.util.List;

/**
 * @author lujingpo
 * @date 2021-06-12
 */
@Data
public class SkuOrder {


    /**
     * 质检状态;0-无需质检;1-可质检;2-必须质检
     * example: "质检状态"
     */
    @JSONField(name = "quality_inspection_status")
    private Long qualityInspectionStatus;

    /**
     * 优惠信息
     * example: ""
     */
    @JSONField(name = "promotion_detail")
    private PromotionDetail_4_4 promotionDetail;

    /**
     * 活动信息
     * 其中活动金额会体现到商品价格中（比如商品原价是100元，参加活动优惠20元，那商品现价就为80元）
     * example: ""
     */
    @JSONField(name = "campaign_info")
    private List<CampaignInfoItem> campaignInfo;

    /**
     * 商品单标签
     * example: ""
     */
    @JSONField(name = "sku_order_tag_ui")
    private List<SkuOrderTagUiItem> skuOrderTagUi;

    /**
     * 核销信息
     * example: ""
     */
    @JSONField(name = "writeoff_info")
    private List<WriteoffInfoItem> writeoffInfo;

    /**
     * 关联订单
     * example: ""
     */
    @JSONField(name = "relation_order")
    private RelationOrder relationOrder;

    /**
     * 红包优惠金额（单位：分）
     * example: "100"
     */
    @JSONField(name = "promotion_redpack_amount")
    private Long promotionRedpackAmount;

    /**
     * 平台红包优惠金额（单位：分），属于平台的红包的优惠金额
     * example: "100"
     */
    @JSONField(name = "promotion_redpack_platform_amount")
    private Long promotionRedpackPlatformAmount;

    /**
     * 达人红包优惠金额（单位：分），属于达人的红包的优惠金额
     * example: "100"
     */
    @JSONField(name = "promotion_redpack_talent_amount")
    private Long promotionRedpackTalentAmount;

    /**
     * 1:邮寄，2:自提
     * example: "1"
     */
    @JSONField(name = "receive_type")
    private Long receiveType;

    /**
     * 是否需要上传序列号，用于判断发货时是否需要上传商品序列号（IMEI码或SN码），true 表示是3C数码商品，需要上传序列号
     * example: "true"
     */
    @JSONField(name = "need_serial_number")
    private Boolean needSerialNumber;

    /**
     * 广告来源，video： 短视频  live：直播
     * example: "live"
     */
    @JSONField(name = "ad_env_type")
    private String adEnvType;

    /**
     * 商品ID，字符串类型
     * example: "3473196049974326153"
     */
    @JSONField(name = "product_id_str")
    private String productIdStr;

    /**
     * 预约发货时间
     * example: "1617355413"
     */
    @JSONField(name = "appointment_ship_time")
    private Long appointmentShipTime;

    /**
     * 直播房间ID，字符串类型
     * example: "1473196049974326153"
     */
    @JSONField(name = "room_id_str")
    private String roomIdStr;

    /**
     * 绑定类型  MASTER-主品单  FREE-免费赠品
     * example: "FREE"
     */
    @JSONField(name = "given_product_type")
    private String givenProductType;

    /**
     * 废弃，使用master_sku_order_id_list；绑定的主品sku单单号 如果given_product_type=FREE 则master_sku_order_id为主品sku单单号
     * example: "1"
     */
    @JSONField(name = "master_sku_order_id")
    private String masterSkuOrderId;

    /**
     * 商品卡券基本信息
     * example: ""
     */
    @JSONField(name = "card_voucher")
    private CardVoucher cardVoucher;

    /**
     * 组套商品子商品列表
     * example: ""
     */
    @JSONField(name = "bundle_sku_info")
    private List<BundleSkuInfoItem> bundleSkuInfo;

    /**
     * 当order_type=2，order_type_desc为虚拟订单时该参数会返回信息
     * example: ""
     */
    @JSONField(name = "account_list")
    private AccountList accountList;

    /**
     * 定制商品信息
     * example: ""
     */
    @JSONField(name = "sku_customization_info")
    private List<SkuCustomizationInfoItem> skuCustomizationInfo;

    /**
     * 是否是补贴品订单
     * example: "false"
     */
    @JSONField(name = "is_activity")
    private Boolean isActivity;

    /**
     * 作者（达人）承担金额（单位：分），订单参与活动和优惠中作者（达人）承担部分的总金额
     * example: "100"
     */
    @JSONField(name = "author_cost_amount")
    private Long authorCostAmount;

    /**
     * 仅平台承担金额（单位：分），订单参与活动和优惠中平台承担部分的总金额
     * example: "100"
     */
    @JSONField(name = "only_platform_cost_amount")
    private Long onlyPlatformCostAmount;

    /**
     * 履约时效信息(json串)
     * example: "{identifier:R#2022-07-13,receipt:{FulfillDecisionId:7117833576879604004,LatestPushTime:1657533600,PromiseTag:ENUM:NEXT2_DAY,ExpReceiptTime:1657727999,RealWarehouseCode:101}}"
     */
    @JSONField(name = "promise_info")
    private String promiseInfo;

    /**
     * 门店信息
     * example: ""
     */
    @JSONField(name = "store_info")
    private StoreInfo storeInfo;

    /**
     * 收件人姓名（脱敏后）
     * example: "王**"
     */
    @JSONField(name = "mask_post_receiver")
    private String maskPostReceiver;

    /**
     * 收件人电话（脱敏后）
     * example: "13*******35"
     */
    @JSONField(name = "mask_post_tel")
    private String maskPostTel;

    /**
     * 收件人地址（脱敏后）
     * example: ""
     */
    @JSONField(name = "mask_post_addr")
    private MaskPostAddr maskPostAddr;

    /**
     * 合约信息
     * example: ""
     */
    @JSONField(name = "contract_info")
    private ContractInfo contractInfo;

    /**
     * 绑定的主品sku单单号 如果given_product_type=FREE 则master_sku_order_id_list为主品sku单单号
     * example: "[4781320682406083640]"
     */
    @JSONField(name = "master_sku_order_id_list")
    private List<String> masterSkuOrderIdList;

    /**
     * 赠品活动信息
     * example: ""
     */
    @JSONField(name = "given_product_activity_info")
    private GivenProductActivityInfo givenProductActivityInfo;

    /**
     * 质检信息
     * example: ""
     */
    @JSONField(name = "quality_check_info")
    private QualityCheckInfo qualityCheckInfo;

    /**
     * sku云仓择仓信息（云仓业务使用，非商品区域仓功能使用）
     * example: ""
     */
    @JSONField(name = "sku_yun_warehouse_info")
    private List<SkuYunWarehouseInfoItem> skuYunWarehouseInfo;

    /**
     * 税费
     * example: "8"
     */
    @JSONField(name = "tax_amount")
    private Long taxAmount;

    /**
     *
     */
    @JSONField(name = "after_sale_info")
    private AfterSaleInfo afterSaleInfo;

    /**
     * 小程序id
     */
    @JSONField(name = "app_id")
    private Integer appId;

    /**
     * 直播主播id（达人）
     */
    @JSONField(name = "author_id")
    private Long authorId;

    /**
     * 直播主播名称
     */
    @JSONField(name = "author_name")
    private String authorName;

    /**
     * 下单端
     * <p/>
     * 可选值：
     * <ul>
     * <li>0-站外</li>
     * <li>1-火山</li>
     * <li>2-抖音等</li>
     * </ul>
     */
    @JSONField(name = "b_type")
    private Integer bType;

    /**
     * 下单端描述
     */
    @JSONField(name = "b_type_desc")
    private String bTypeDesc;

    /**
     * 业务来源
     * <p/>
     * 可选值：
     * <ul>
     * <li>1-鲁班</li>
     * <li>2-小店</li>
     * <li>3-好好学习等</li>
     * </ul>
     */
    private Integer biz;

    /**
     * 业务来源描述
     */
    @JSONField(name = "biz_desc")
    private String bizDesc;

    /**
     * C端流量来源业务类型
     * <p/>
     * 可选值：
     * <ul>
     * <li>1:"鲁班广告"</li>
     * <li>2: "联盟"</li>
     * <li>4:return "商城"</li>
     * <li>8:"自主经营"</li>
     * <li>10:"线索/表单收集类广告"</li>
     * <li>12: "抖音门店"</li>
     * <li>14:"抖+"</li>
     * <li>15: "穿山甲"</li>
     * <li>16:"服务市场"</li>
     * <li>18: "服务市场外包客服"</li>
     * </ul>
     */
    @JSONField(name = "c_biz")
    private Integer cBiz;

    /**
     * C端流量来源业务类型描述
     */
    @JSONField(name = "c_biz_desc")
    private String cBizDesc;

    /**
     * 取消原因
     */
    @JSONField(name = "cancel_reason")
    private String cancelReason;

    /**
     * 支付流水号
     */
    @JSONField(name = "channel_payment_no")
    private String channelPaymentNo;

    /**
     * 广告id
     */
    private Long cid;

    /**
     * 商家编码
     */
    private String code;

    /**
     * 用户确认收货时间
     * <p/>
     * 秒级时间戳
     */
    @JSONField(name = "confirm_receipt_time")
    private Long confirmReceiptTime;

    /**
     * 内容id
     */
    @JSONField(name = "content_id")
    private String contentId;

    /**
     * 下单时间
     * <p/>
     * 秒级时间戳
     */
    @JSONField(name = "create_time")
    private Long createTime;

    /**
     * 密文出参
     * <p/>
     * 参考文档: <a href="https://bytedance.feishu.cn/docs/doccnJNKML3jOzFgjJitzXy61lh#">加解密指南</a>
     */
    @JSONField(name = "encrypt_post_receiver")
    private String encryptPostReceiver;

    /**
     * 收件人电话
     *
     */
    @JSONField(name = "encrypt_post_tel")
    private String encryptPostTel;

    /**
     * 预计发货时间
     * <p/>
     * 秒级时间戳
     */
    @JSONField(name = "exp_ship_time")
    private Long expShipTime;

    /**
     * 订单完成时间
     * <p/>
     * 秒级时间戳
     */
    @JSONField(name = "finish_time")
    private Long finishTime;

    /**
     * 一级类目
     */
    @JSONField(name = "first_cid")
    private Integer firstCid;

    /**
     * 四级类目
     */
    @JSONField(name = "fourth_cid")
    private Integer fourthCid;

    /**
     * 商品类型
     */
    @JSONField(name = "goods_type")
    private Integer goodsType;

    /**
     * 是否包税
     */
    @JSONField(name = "has_tax")
    private Boolean hasTax;

    /**
     * 仓储列表
     */
    @JSONField(name = "inventory_list")
    private List<Inventory> inventoryList;

    /**
     * 库存类型
     * 普通库存/区域库存
     *
     * @deprecated 废弃，使用inventory_list
     */
    @JSONField(name = "inventory_type")
    private String inventoryType;

    /**
     * 库存类型描述
     *
     * @deprecated 废弃，使用inventory_list
     */
    @JSONField(name = "inventory_type_desc")
    private String inventoryTypeDesc;

    /**
     * 是否评价
     * <p/>
     * 可选值：
     * <ul>
     *    <li>1已评价</li>
     *    <li>0未评价</li>
     * </ul>
     *
     */
    @JSONField(name = "is_comment")
    private Integer isComment;

    /**
     * 商品件数
     */
    @JSONField(name = "item_num")
    private Integer itemNum;

    /**
     * 物流收货时间
     * <p/>
     * 秒级时间戳
     */
    @JSONField(name = "logistics_receipt_time")
    private Long logisticsReceiptTime;

    /**
     * 主流程状态
     * <p/>
     * Example: 103
     * <br/>
     * 参考文档：<a href="https://op.jinritemai.com/docs/guide-docs/33/136">订单状态机</a>
     */
    @JSONField(name = "main_status")
    private Integer mainStatus;

    /**
     * 主流程状态描述
     * <p/>
     * Example: 部分支付
     */
    @JSONField(name = "main_status_desc")
    private String mainStatusDesc;

    /**
     * 改价金额变化量
     * <p/>
     * 单位：分
     */
    @JSONField(name = "modify_amount")
    private Integer modifyAmount;

    /**
     * 改价运费金额变化量
     * <p/>
     * 单位：分
     */
    @JSONField(name = "modify_post_amount")
    private Integer modifyPostAmount;

    /**
     * 订单金额（分）
     */
    @JSONField(name = "order_amount")
    private Integer orderAmount;

    /**
     * 订单过期时间
     * <p/>
     * 单位：秒
     * Example: 1800
     */
    @JSONField(name = "order_expire_time")
    private Long orderExpireTime;

    /**
     * 店铺订单号
     */
    @JSONField(name = "order_id")
    private String orderId;

    /**
     * 订单层级
     */
    @JSONField(name = "order_level")
    private Integer orderLevel;

    /**
     * 订单状态
     * <p/>
     * 参考文档：<a href="https://op.jinritemai.com/docs/guide-docs/33/136">订单状态机</a>
     */
    @JSONField(name = "order_status")
    private Integer orderStatus;

    /**
     * 订单状态描述
     */
    @JSONField(name = "order_status_desc")
    private String orderStatusDesc;

    /**
     * 订类型
     * <p/>
     * <blockquote>
     * <ul>
     * <li>0-普通订单</li>
     * <li>2-虚拟订单</li>
     * <li>4-平台券码</li>
     * <li>5-商家券码</li>
     * </ul>
     * </blockquote>
     *
     */
    @JSONField(name = "order_type")
    private Integer orderType;

    /**
     * 订单类型描述
     */
    @JSONField(name = "order_type_desc")
    private String orderTypeDesc;

    /**
     * 商品现价
     * <p/>
     * 单位：分
     */
    @JSONField(name = "origin_amount")
    private Integer originAmount;

    /**
     * 流量来源id
     *
     */
    @JSONField(name = "origin_id")
    private String originId;

    /**
     * 商品外部编码
     */
    @JSONField(name = "out_product_id")
    private String outProductId;

    /**
     * 外部skuId
     */
    @JSONField(name = "out_sku_id")
    private String outSkuId;

    /**
     * 外部仓id，
     * @deprecated 废弃，使用inventory_list
     */
    @JSONField(name = "out_warehouse_ids")
    private List<String> outWarehouseIds;

    /**
     * 广告展示页ID
     */
    @JSONField(name = "page_id")
    private Integer pageId;

    /**
     * 父订单号（店铺订单号）
     */
    @JSONField(name = "parent_order_id")
    private String parentOrderId;

    /**
     * 支付金额（分）
     */
    @JSONField(name = "pay_amount")
    private Integer payAmount;

    /**
     * 支付时间
     * <p/>
     * 秒级时间戳
     */
    @JSONField(name = "pay_time")
    private Long payTime;

    /**
     * 支付类型
     * <p/>
     * <ul>
     * <li>0-货到付款</li>
     * <li>1-微信</li>
     * <li>2-支付宝</li>
     * </ul>
     */
    @JSONField(name = "pay_type")
    private Integer payType;

    /**
     * 平台优惠金额平台承担部分
     * <p/>
     * 单位：分
     */
    @JSONField(name = "platform_cost_amount")
    private Integer platformCostAmount;

    /**
     * 收件人地址
     */
    @JSONField(name = "post_addr")
    private PostAddr postAddr;

    /**
     * 快递费（分）
     */
    @JSONField(name = "post_amount")
    private Integer postAmount;

    /**
     * 运费险金额
     */
    @JSONField(name = "post_insurance_amount")
    private Integer postInsuranceAmount;

    /**
     * 收件人姓名
     * @deprecated 数据加密完成之后，需要使用 {@link ShopOrder#getEncryptPostReceiver()}
     */
    @JSONField(name = "post_receiver")
    private String postReceiver;

    /**
     * 收件人手机号
     * @deprecated 数据加密完成之后，需要使用 {@link ShopOrder#getEncryptPostTel()}
     */
    @JSONField(name = "post_tel")
    private String postTel;

    /**
     * 预售类型
     * <P/>
     * 可选值：
     * <ul>
     *     <li>0 现货类型</li>
     *     <li>1 全款预售</li>
     *     <li>2 阶梯发货</li>
     * </ul>
     *
     */
    @JSONField(name = "pre_sale_type")
    private Integer preSaleType;

    /**
     * 商品ID
     */
    @JSONField(name = "product_id")
    private Long productId;

    /**
     * 商品名称
     */
    @JSONField(name = "product_name")
    private String productName;

    /**
     * 商品图片
     */
    @JSONField(name = "product_pic")
    private String productPic;

    /**
     * 单优惠总金额
     * <p/>
     * 计算方法：
     * <blockquote>单优惠总金额 = 店铺优惠金额+ 平台优惠金额+ 达人优惠金额+ 支付优惠金额</blockquote>
     */
    @JSONField(name = "promotion_amount")
    private Integer promotionAmount;

    /**
     * 支付优惠金额
     */
    @JSONField(name = "promotion_pay_amount")
    private Integer promotionPayAmount;

    /**
     * 平台优惠金额
     */
    @JSONField(name = "promotion_platform_amount")
    private Integer promotionPlatformAmount;

    /**
     * 店铺优惠金额
     */
    @JSONField(name = "promotion_shop_amount")
    private Integer promotionShopAmount;

    /**
     * 达人优惠金额
     */
    @JSONField(name = "promotion_talent_amount")
    private Integer promotionTalentAmount;

    /**
     * 库存扣减方式
     */
    @JSONField(name = "reduce_stock_type")
    private Integer reduceStockType;

    /**
     * 库存扣减方式名称
     */
    @JSONField(name = "reduce_stock_type_desc")
    private String reduceStockTypeDesc;

    /**
     * 直播间id
     */
    @JSONField(name = "room_id")
    private Long roomId;

    /**
     * 二级类目
     */
    @JSONField(name = "second_cid")
    private Integer secondCid;

    /**
     * 流量来源
     * <p/>
     * <ul>
     *    <li>1-鲁班广告</li>
     *    <li>2-联盟</li>
     *    <li>3-商城</li>
     *    <li>4-自主经营</li>
     *    <li>5-线索通支付表单</li>
     *    <li>6-抖音门店</li>
     *    <li>7-抖+</li>
     *    <li>8-穿山甲</li>
     * </ul>
     *
     */
    @JSONField(name = "send_pay")
    private Integer sendPay;

    /**
     * 流量来源描述
     */
    @JSONField(name = "send_pay_desc")
    private String sendPayDesc;

    /**
     * 发货时间
     * <p/>
     * 秒级时间戳
     */
    @JSONField(name = "ship_time")
    private Long shipTime;

    /**
     * 平台优惠金额卖家承担部分
     */
    @JSONField(name = "shop_cost_amount")
    private Integer shopCostAmount;

    /**
     * 商品skuId
     */
    @JSONField(name = "sku_id")
    private Long skuId;

    /**
     * 商品来源平台
     */
    @JSONField(name = "source_platform")
    private String sourcePlatform;

    /**
     * 规格信息
     */
    private List<SkuSpecs> spec;

    /**
     * 下单场景
     * <p/>
     * 可选值：
     * <ul>
     * <li>0 未知</li>
     * <li>1 app</li>
     * <li>2 小程序</li>
     * <li>3 H5</li>
     * </ul>
     */
    @JSONField(name = "sub_b_type")
    private Integer subBType;

    /**
     * 下单场景描述
     */
    @JSONField(name = "sub_b_type_desc")
    private String subBTypeDesc;

    /**
     * 商品现价*件数
     */
    @JSONField(name = "sum_amount")
    private Integer sumAmount;

    /**
     * sku外部供应商编码
     */
    @JSONField(name = "supplier_id")
    private String supplierId;

    /**
     * 下单来源
     * <p/>
     * 可选值：
     * <ul>
     * <li>0-其他</li>
     * <li>1-直播间</li>
     * </ul>
     */
    @JSONField(name = "theme_type")
    private String themeType;

    /**
     * 下单来源描述
     */
    @JSONField(name = "theme_type_desc")
    private String themeTypeDesc;

    /**
     * 三级类目
     */
    @JSONField(name = "third_cid")
    private Integer thirdCid;

    /**
     * 交易类型
     * <p/>
     * 可选值：
     * <ul>
     * <li>1 拼团订单</li>
     * <li>2 定金预售</li>
     * </ul>
     */
    @JSONField(name = "trade_type")
    private Integer tradeType;

    /**
     * 交易类型描述
     */
    @JSONField(name = "trade_type_desc")
    private String tradeTypeDesc;

    /**
     * 订单更新时间
     * <p/>
     * 秒级时间戳
     */
    @JSONField(name = "update_time")
    private Long updateTime;

    /**
     * 视频id
     */
    @JSONField(name = "video_id")
    private String videoId;

    /**
     * 仓id
     * @deprecated 废弃，使用inventory_list
     */
    @JSONField(name = "warehouse_ids")
    private List<String> warehouseIds;



}
