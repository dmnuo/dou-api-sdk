package com.moming.douapisdk.domain;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;

/**
 * 成本分摊
 *
 * @author lujingpo
 * @since 2023/5/6
 */
@Data
public class ShareDiscountCost {

    /**
     * 平台承担金额（单位：分）
     * example = "100"
     */
    @JSONField(name = "platform_cost")
    private Long platformCost;

    /**
     * 商家承担金额（单位：分）
     * example = "100"
     */
    @JSONField(name = "shop_cost")
    private Long shopCost;

    /**
     * 作者承担金额（单位：分）
     * example = "100"
     */
    @JSONField(name = "author_cost")
    private Long authorCost;

}
