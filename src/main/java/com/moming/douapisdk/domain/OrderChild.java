package com.moming.douapisdk.domain;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.List;

/**
 * @author tianzong
 * @date 2020/7/23
 */
@Data
public class OrderChild {

    @JSONField(name = "adjust_amount")
    private Integer adjustAmount;

    @JSONField(name = "adjust_post_amount")
    private Integer adjustPostAmount;

    @JSONField(name = "alliance_info")
    private AllianceInfo allianceInfo;

    @JSONField(name = "author_id")
    private Long authorId;

    @JSONField(name = "buyer_words")
    private String buyerWords;

    @JSONField(name = "c_type")
    private String cType;

    @JSONField(name = "b_type")
    private String bType;

    @JSONField(name = "campaign_id")
    private String campaignId;

    @JSONField(name = "cancel_reason")
    private String cancelReason;

    private String code;

    /**
     * 该子订单所购买的sku的售价
     */
    @JSONField(name = "combo_amount")
    private Integer comboAmount;

    @JSONField(name = "combo_id")
    private Long comboId;

    /**
     * 该子订单所购买的sku的数量
     */
    @JSONField(name = "combo_num")
    private Integer comboNum;

    @JSONField(name = "cos_ratio")
    private String cosRatio;

    @JSONField(name = "coupon_amount")
    private Integer couponAmount;

    @JSONField(name = "coupon_meta_id")
    private String couponMetaId;

    @JSONField(name = "create_time")
    private String createTime;

    @JSONField(name = "is_comment")
    private String isComment;

    @JSONField(name = "logistics_code")
    private String logisticsCode;

    @JSONField(name = "logistics_id")
    private Integer logisticsId;

    @JSONField(name = "logistics_time")
    private String logisticsTime;

    @JSONField(name = "order_id")
    private String orderId;

    @JSONField(name = "order_status")
    private Integer orderStatus;

    @JSONField(name = "order_type")
    private Integer orderType;

    @JSONField(name = "out_product_id")
    private Long outProductId;

    @JSONField(name = "out_sku_id")
    private Long outSkuId;

    @JSONField(name = "pay_type")
    private Integer payType;

    @JSONField(name = "pay_time")
    private String payTime;

    private String pid;

    @JSONField(name = "post_addr")
    private PostAddr postAddr;

    @JSONField(name = "post_amount")
    private Integer postAmount;

    @JSONField(name = "post_code")
    private String postCode;

    @JSONField(name = "post_receiver")
    private String postReceiver;

    @JSONField(name = "post_tel")
    private String postTel;

    @JSONField(name = "product_id")
    private String productId;

    @JSONField(name = "product_name")
    private String productName;

    @JSONField(name = "product_pic")
    private String productPic;

    /**
     * 收货时间。未收货时为"0"，已发货返回秒级时间戳
     */
    @JSONField(name = "receipt_time")
    private String receiptTime;

    @JSONField(name = "seller_remark_stars")
    private Integer sellerRemarkStars;

    @JSONField(name = "seller_words")
    private String sellerWords;

    @JSONField(name = "shop_coupon_amount")
    private Integer shopCouponAmount;

    @JSONField(name = "shop_id")
    private Integer shopId;

    /**
     * 子订单实付金额（不包含运费）
     */
    @JSONField(name = "total_amount")
    private Integer totalAmount;

    @JSONField(name = "update_time")
    private Integer updateTime;

    @JSONField(name = "urge_cnt")
    private Integer urgeCnt;

    @JSONField(name = "user_name")
    private String userName;

    @JSONField(name = "exp_ship_time")
    private Integer expShipTime;

    @JSONField(name = "final_status")
    private Integer finalStatus;

    @JSONField(name = "campaign_info")
    private List<CampaignInfo> campaignInfo;

    @JSONField(name = "coupon_info")
    private List<CouponInfo> couponInfo;

    @JSONField(name = "spec_desc")
    private List<SpecDesc> specDesc;


    @Setter
    @Getter
    @ToString
    public static class CampaignInfo {
        /**
         * campaign_id : 123123
         * title : 【好货】123===
         */

        @JSONField(name = "campaign_id")
        private Long campaignId;
        private String title;


    }

    @Data
    @ToString
    public static class SpecDesc {
        /**
         * name : 颜色分类
         * value : 正方形（送土豪金勺+杯盖）
         */

        private String name;
        private String value;

    }

    @Data
    @ToString
    public static class AllianceInfo {
        @JSONField(name = "author_account")
        private String authorAccount;

        @JSONField(name = "commission_rate")
        private int commissionRate;

        @JSONField(name = "short_id")
        private String shortId;

        @JSONField(name = "estimated_commission")
        private Integer estimatedCommission;

        @JSONField(name = "real_commission")
        private Integer realCommission;
    }
}
