package com.moming.douapisdk.domain;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;

/**
 * 定制图片信息
 *
 * @author lujingpo
 * @since 2023/5/6
 */
@Data
public class PicItem {

    /**
     * id
     * example = "1"
     */
    @JSONField(name = "id")
    private Long id;

    /**
     * url
     * example = "https://www.xxx.xxx"
     */
    @JSONField(name = "url")
    private String url;
}
