package com.moming.douapisdk.domain;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;

/**
 * 店铺单标签
 *
 * @author lujingpo
 * @since 2023/5/6
 */
@Data
public class ShopOrderTagUiItem {

    /**
     * 标签key
     * example:"标签key"
     */
    @JSONField(name = "key")
    private String key;

    /**
     * 标签名称
     * example:"推荐音尊达"
     */
    @JSONField(name = "text")
    private String text;

    /**
     * 帮助文档
     * example:"帮助文档"
     */
    @JSONField(name = "help_doc")
    private String helpDoc;
}
