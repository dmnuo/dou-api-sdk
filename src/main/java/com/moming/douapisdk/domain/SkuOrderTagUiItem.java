package com.moming.douapisdk.domain;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;

/**
 * 商品单标签
 *
 * @author lujingpo
 * @since 2023/5/6
 */
@Data
public class SkuOrderTagUiItem {

    /**
     * 标签key
     * example = "pre_sale_label"
     */
    @JSONField(name = "key")
    private String key;

    /**
     * 标签文案
     * example = "全款预售"
     */
    @JSONField(name = "text")
    private String text;

    /**
     * 标签备注文案
     * example = "该商品需要送到质检中心，质检完成后发给用户"
     */
    @JSONField(name = "hover_text")
    private String hoverText;

    /**
     * 标签类型，如颜色
     * example = "orange"
     */
    @JSONField(name = "tag_type")
    private String tagType;

    /**
     * 帮助文档
     * example = "https://school.jinritemai.com/doudian/web/article/101835?from=shop_article"
     */
    @JSONField(name = "help_doc")
    private String helpDoc;

    /**
     * 排序
     * example = "1"
     */
    @JSONField(name = "sort")
    private Long sort;

    /**
     * 标签其他信息
     * example = "{key:value}"
     */
    @JSONField(name = "extra")
    private String extra;

}
