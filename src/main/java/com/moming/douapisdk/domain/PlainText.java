package com.moming.douapisdk.domain;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;

/**
 * 明文
 * @author LangYu
 * @date 2021/7/10
 */
@Data
public class PlainText {
    /**
     * 明文
     */
    @JSONField(name = "plain_text")
    private String plainText;

    /**
     * 加密类型；
     * 1地址加密
     * 2姓名加密
     * 3电话加密
     */
    @JSONField(name = "encrypt_type")
    private Integer encryptType;

}
