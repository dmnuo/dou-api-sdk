package com.moming.douapisdk.constant;

import lombok.Getter;

/**
 * <pre>
 *     抖店错误码
 *     参考文档：<a href="https://op.jinritemai.com/docs/guide-docs/10/23">返回错误码列表</a>
 * </pre>
 *
 * @author lujingpo
 * @date 2021/2/3
 */
@Getter
public enum DouYinErrorCodeEnum {

    CODE_10000(10000, "success"),
    CODE_10001(10001, "请求部分失败"),
    CODE_20000(20000, "服务不可用"),
    CODE_20001(20001, "内部服务超时"),
    CODE_30001(30001, "操作权限不足"),
    CODE_30002(30002, "请求来源IP不可信，请检查IP白名单"),
    CODE_40001(40001, "缺少必选参数（平台校验）"),
    CODE_40002(40002, "缺少必选参数（业务校验）"),
    CODE_40003(40003, "非法的参数（平台校验）"),
    CODE_40004(40004, "非法的参数（业务校验）"),
    CODE_50001(50001, "平台处理失败"),
    CODE_50002(50002, "业务处理失败"),
    CODE_60000(60000, "触发限流，请稍后重试"),
    CODE_70000(70000, "接口服务已下线"),
    CODE_80000(80000, "安全错误"),
    CODE_90000(90000, "其他异常"),
    ;



    DouYinErrorCodeEnum(int code, String message) {
        this.code = code;
        this.message = message;
    }

    /**
     * 错误码
     */
    private final int code;

    /**
     * 错误信息
     */
    private final String message;
}
