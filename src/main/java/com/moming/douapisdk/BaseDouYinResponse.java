package com.moming.douapisdk;


import lombok.Data;

import java.io.Serializable;

/**
 * @author tianzong
 * @date 2020/7/23
 */
@Data
public abstract class BaseDouYinResponse implements Serializable {

    private static final long serialVersionUID = -9086019471343707227L;
}
