package com.moming.douapisdk.response.order;

import com.alibaba.fastjson.annotation.JSONField;
import com.moming.douapisdk.BaseDouYinResponse;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

/**
 * @author lujingpo
 * @date 2022/7/6
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class GetSettleBillDetailV3Response extends BaseDouYinResponse {
    private static final long serialVersionUID = 3557343171948553495L;

    /**
     * 返回code 100000为成功，其他为失败
     *
     */
    private String code;

    /**
     * 返回信息描述，失败状态下会有失败描述
     */
    @JSONField(name = "code_msg")
    private String codeMsg;

    /**
     * 订单流水明细列表
     */
    private List<SettleBillDetailV3> data;

    /**
     * 请求的size
     */
    @JSONField(name = "data_size")
    private String dataSize;

    /**
     * 判断查询是否结束。0 未结束, 1 结束。
     * 未结束时，需要
     * 把next_start_index作为下一次请求的start_index,next_start_time作为下一次请求的start_time
     */
    @JSONField(name = "is_end")
    private String isEnd;

    /**
     * 下一次查询start_index
     */
    @JSONField(name = "next_start_index")
    private String nextStartIndex;

    /**
     * 下一次查询start_time
     */
    @JSONField(name = "next_start_time")
    private String nextStartTime;

    /**
     * 请求的size
     */
    private String size;


    @Data
    public static class SettleBillDetailV3 {

        /**
         * 达人补贴（分）
         */
        @JSONField(name = "author_coupon")
        private Long authorCoupon;
        /**
         * 直播间站外推广（分）
         */
        @JSONField(name = "channel_promotion_fee")
        private Long channelPromotionFee;
        /**
         * 团长服务费（分）
         */
        @JSONField(name = "colonel_service_fee")
        private Long colonelServiceFee;
        /**
         * 佣金（分）
         */
        private Long commission;
        /**
         * 业务类型: 鲁班广告、值点商城、精选联盟、小店自卖等
         */
        @JSONField(name = "flow_type_desc")
        private String flowTypeDesc;
        /**
         * 渠道分成（分）
         */
        @JSONField(name = "good_learn_channel_fee")
        private Long goodLearnChannelFee;
        /**
         * 商品数量
         */
        @JSONField(name = "goods_count")
        private Integer goodsCount;
        /**
         * 是否包含结算前退款 0：不包含 1：包含
         */
        @JSONField(name = "is_contains_refund_before_settle")
        private Integer isContainsRefundBeforeSettle;

        /**
         * 子订单号
         */
        @JSONField(name = "order_id")
        private String orderId;
        /**
         * 下单时间
         */
        @JSONField(name = "order_time")
        private String orderTime;
        /**
         * 订单类型：普通订单、尾款(尾款已支付)、尾款(已退款)、定金(已退款)、定金(尾款已支付)、定金(尾款未支付)
         */
        @JSONField(name = "order_type")
        private String orderType;

        /**
         * 其他分成(学浪)（分）
         */
        @JSONField(name = "other_sharing_amount")
        private String otherSharingAmount;
        /**
         * 货款结算对应的账户类型： “聚合账户”“微信”“支付宝”“微信升级前”“合众支付”等
         */
        @JSONField(name = "pay_type_desc")
        private String payTypeDesc;
        /**
         * 平台补贴（分）
         */
        @JSONField(name = "platform_coupon")
        private Long platformCoupon;
        /**
         * 平台服务费（分）
         */
        @JSONField(name = "platform_service_fee")
        private Long platformServiceFee;
        /**
         * 运费（分）
         */
        @JSONField(name = "post_amount")
        private Long postAmount;
        /**
         * 商品id
         */
        @JSONField(name = "product_id")
        private String productId;
        /**
         * 用户实付（分）
         */
        @JSONField(name = "real_pay_amount")
        private Long realPayAmount;
        /**
         * 结算前退款金额（分） （结算前退货+运费-店铺券）
         */
        @JSONField(name = "refund_before_settle")
        private Long refundBeforeSettle;

        /**
         * 备注
         */
        private String remark;
        /**
         * 结算单号
         */
        @JSONField(name = "request_no")
        private String requestNo;
        /**
         * 商家实收（分）
         */
        @JSONField(name = "settle_amount")
        private Long settleAmount;
        /**
         * 结算时间
         */
        @JSONField(name = "settle_time")
        private String settleTime;
        /**
         * 店铺券（分）
         */
        @JSONField(name = "shop_coupon")
        private Long shopCoupon;
        /**
         * 订单号
         */
        @JSONField(name = "shop_order_id")
        private String shopOrderId;
        /**
         * 订单总价（分）
         */
        @JSONField(name = "total_amount")
        private Long totalAmount;
        /**
         * 商品总价（分）
         */
        @JSONField(name = "total_goods_amount")
        private Long totalGoodsAmount;
        /**
         * 收入合计（分）
         */
        @JSONField(name = "total_income")
        private Long totalIncome;
        /**
         * 其他分成(学浪)（分）
         */
        @JSONField(name = "total_outcome")
        private Long totalOutcome;
        /**
         * 结算单类型
         * 0 ：已结算
         * 1 ：结算后退款-原路退回
         * 2： 保证金退款-支出退回
         * 3： 结算后退款-非原路退回
         */
        @JSONField(name = "trade_type")
        private Integer tradeType;
        /**
         * DOU分期营销补贴（分）
         */
        @JSONField(name = "zr_pay_promotion")
        private Long zrPayPromotion;

        /**
         * 抖音支付补贴（分）
         */
        @JSONField(name = "zt_pay_promotion")
        private String ztPayPromotion;
    }
}
