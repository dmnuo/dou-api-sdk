package com.moming.douapisdk.response;

import com.moming.douapisdk.BaseDouYinResponse;
import com.moming.douapisdk.domain.DouYinProduct;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

/**
 *
 * 商品列表返回值
 * @author lujingpo
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class ProductListResponse extends BaseDouYinResponse {

    private static final long serialVersionUID = 352260591893748608L;

    private int all;
    private int allPages;
    private int count;
    private int currentPage;
    private List<DouYinProduct> data;
    private int pageSize;

}
