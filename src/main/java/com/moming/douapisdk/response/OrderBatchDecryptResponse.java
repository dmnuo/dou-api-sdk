package com.moming.douapisdk.response;

import com.alibaba.fastjson.annotation.JSONField;
import com.moming.douapisdk.BaseDouYinResponse;
import com.moming.douapisdk.domain.CustomErr;
import com.moming.douapisdk.domain.DecryptInfo;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

/**
 * 脱敏信息
 * @author LangYu
 * @date 2021/7/10
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class OrderBatchDecryptResponse extends BaseDouYinResponse {

    /**
     * 脱敏信息列表
     */
    @JSONField(name = "decrypt_infos")
    private List<DecryptInfo> decryptInfos;

    /**
     * 业务错误
     */
    @JSONField(name = "custom_err")
    private CustomErr customErr;

}
