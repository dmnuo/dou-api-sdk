package com.moming.douapisdk.response.aftersale;

import com.alibaba.fastjson.annotation.JSONField;
import com.moming.douapisdk.BaseDouYinResponse;
import com.moming.douapisdk.domain.PostAddr;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

/**
 * @author lujingpo
 * @date 2022/8/23
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class AfterSaleDetailResponse extends BaseDouYinResponse {
    private static final long serialVersionUID = 3508946079867883409L;


    /**
     * 售后单关联订单信息
     */
    @JSONField(name = "order_info")
    private OrderInfo orderInfo;

    /**
     * 售后单及相关信息
     */
    @JSONField(name = "process_info")
    private ProcessInfo processInfo;

    /**
     * 售后单关联订单信息
     */
    @Data
    public static class OrderInfo {
        /**
         * 店铺单ID
         */
        @JSONField(name = "shop_order_id")
        private Long shopOrderId;

        /**
         * sku单信息
         */
        @JSONField(name = "sku_order_infos")
        private List<SkuOrderInfos> skuOrderInfos;

        /**
         * sku单信息
         */
        @Data
        public static class SkuOrderInfos {

            /**
             * 商品单对应的售后数量
             */
            @JSONField( name = "after_sale_item_count")
            private Long afterSaleItemCount;
            /**
             * 订单创建时间
             */
            @JSONField( name = "create_time")
            private Long createTime;

            /**
             * 赠品信息
             */
            @JSONField( name = "given_sku_details")
            private List<GivenSkuDetails> givenSkuDetails;

            /**
             * 保险及其状态
             */
            @JSONField( name = "insurance_tags")
            private List<AfterSaleListResponse.Tags> insuranceTags;

            /**
             * 是否为跨境业务
             */
            @JSONField( name = "is_oversea_order")
            private Long isOverseaOrder;
            /**
             * 订单件数
             */
            @JSONField( name = "item_quantity")
            private Long itemQuantity;

            /**
             * sku商品总原价（不含优惠）
             */
            @JSONField( name = "item_sum_amount")
            private Long itemSumAmount;
            /**
             * 订单状态
             */
            @JSONField( name = "order_status")
            private Long orderStatus;
            /**
             * 买家实付金额（分）
             */
            @JSONField( name = "pay_amount")
            private Long payAmount;
            /**
             * 支付方式
             */
            @JSONField( name = "pay_type")
            private Long payType;
            /**
             * 买家购买运费（分）
             */
            @JSONField( name = "post_amount")
            private Long postAmount;
            /**
             * 商品ID
             */
            @JSONField( name = "product_id")
            private Long productId;
            /**
             * 商品图片
             */
            @JSONField( name = "product_image")
            private String productImage;
            /**
             * 商品名称
             */
            @JSONField( name = "product_name")
            private String productName;
            /**
             * 优惠总金额
             */
            @JSONField( name = "promotion_amount")
            private Long promotionAmount;

            /**
             * 商家sku自定义编码
             */
            @JSONField( name = "shop_sku_code")
            private String shopSkuCode;

            /**
             * skuID
             */
            @JSONField( name = "sku_id")
            private Long skuId;
            /**
             * sku单ID
             */
            @JSONField( name = "sku_order_id")
            private Long skuOrderId;
            /**
             * 商品实际支付金额
             */
            @JSONField( name = "sku_pay_amount")
            private Long skuPayAmount;
            /**
             * 商品规格信息
             */
            @JSONField( name = "sku_spec")
            private List<AfterSaleListResponse.SkuSpec> skuSpec;

            /**
             * 商品标签
             */
            private List<AfterSaleListResponse.Tags> tags;

            /**
             * 购买税费（分）
             */
            @JSONField( name = "tax_amount")
            private Long taxAmount;

            /**
             * 赠品信息
             */
            @Data
            public static class GivenSkuDetails {

                /**
                 * 赠品名称
                 */
                @JSONField(name = "product_name")
                private String productName;
                /**
                 * 该订单对应的赠品订单
                 */
                @JSONField(name = "sku_order_id")
                private String skuOrderId;

            }
        }
    }

    /**
     * 售后单及相关信息
     */
    @Data
    public static class ProcessInfo {

        /**
         * 售后单信息
         */
        @JSONField( name = "after_sale_info")
        private AfterSaleInfo afterSaleInfo;

        /**
         * 售后标签
         */
        @JSONField( name = "after_sale_service_tag")
        private AfterSaleServiceTags afterSaleServiceTag;

        /**
         * 售后备注
         */
        @JSONField( name = "after_sale_shop_remarks")
        private List<AfterSaleShopRemarks> afterSaleShopRemarks;

        /**
         * 仲裁信息
         */
        @JSONField( name = "arbitrate_info")
        private ArbitrateInfo arbitrateInfo;

        /**
         * 物流信息
         */
        @JSONField( name = "logistics_info")
        private LogisticsInfo logisticsInfo;

        /**
         * 价保详情
         */
        @JSONField( name = "price_protection_detail")
        private PriceProtectionDetail priceProtectionDetail;

        /**
         * 售后单信息
         */
        @Data
        public static class AfterSaleInfo {
            /**
             * 用户申请售后件数
             */
            @JSONField(name = "after_sale_apply_count")
            private Long afterSaleApplyCount;
            /**
             * 售后单ID
             */
            @JSONField(name = "after_sale_id")
            private Long afterSaleId;
            /**
             * 售后状态
             * 6-售后申请；
             * 27-拒绝售后申请；
             * 12-售后成功；
             * 7-售后退货中；
             * 11-售后已发货；
             * 29-售后退货拒绝；
             * 13-【换货返回：换货售后换货商家发货】，【补寄返回：补寄待用户收货】；
             * 14-【换货返回：（换货）售后换货用户收货】，【补寄返回：（补寄）用户已收货】 ；
             * 28-售后失败；
             * 51-订单取消成功；
             * 53-逆向交易已完成；
             *
             */
            @JSONField(name = "after_sale_status")
            private Long afterSaleStatus;

            /**
             * 售后状态文案
             */
            @JSONField(name = "after_sale_status_desc")
            private String afterSaleStatusDesc;

            /**
             * 售后类型
             * 0-退货退款;
             * 1-已发货仅退款;
             * 2-未发货仅退款;
             * 3-换货;
             * 4-系统取消;
             * 5-用户取消;
             * 6-价保;
             * 7-补寄;
             *
             */
            @JSONField(name = "after_sale_type")
            private Long afterSaleType;

            /**
             * 售后单类型文案
             */
            @JSONField(name = "after_sale_type_text")
            private String afterSaleTypeText;

            /**
             * 售后标签
             */
            @JSONField(name = "aftersale_tags")
            private List<AftersaleTags> aftersaleTags;

            /**
             * 售后单申请时间
             */
            @JSONField(name = "apply_time")
            private Long applyTime;

            /**
             * 平台需要回收的金额（分）
             */
            @JSONField(name = "deduction_amount")
            private Long deductionAmount;

            /**
             * 作废的券ID
             */
            @JSONField(name = "disable_coupon_id")
            private String disableCouponId;

            /**
             * 换货、补寄时的收货人名字
             * （只有换货、补寄时，这个字段才会有值），此字段已加密，使用前需要解密
             */
            @JSONField(name = "encrypt_post_receiver")
            private String encryptPostReceiver;

            /**
             * 换货、补寄时的收货人的联系电话
             * （只有换货、补寄时，这个字段才会有值），此字段已加密，使用前需要解密
             */
            @JSONField(name = "encrypt_post_tel_sec")
            private String encryptPostTelSec;
            /**
             * 买家申请退款图片凭证；仅支持图片，最大返回6张图片。
             *
             */
            private String evidence;

            /**
             * 换货SKU信息
             */
            @JSONField(name = "exchange_sku_info")
            private ExchangeSkuInfo exchangeSkuInfo;

            /**
             * 退金币金额
             */
            @JSONField(name = "gold_coin_return_amount")
            private Long goldCoinReturnAmount;

            /**
             * 买家是否收到货，0-表示未收到货；1-表示收到货
             */
            @JSONField(name = "got_pkg")
            private Long gotPkg;

            /**
             * 达人优惠退回金额
             */
            @JSONField(name = "kol_discount_return_amount")
            private Long kolDiscountReturnAmount;

            /**
             * 达人优惠退回状态，枚举：0:待退补贴；1:退补贴成功；2:退补贴失败
             */
            @JSONField(name = "kol_discount_return_status")
            private Long kolDiscountReturnStatus;

            /**
             * 用户需退回件数, 数值为用户申请售后件数 - 商家未发货件数
             */
            @JSONField(name = "need_return_count")
            private Long needReturnCount;

            /**
             * 部分退状态，0为全额退款，1为部分退
             */
            @JSONField(name = "part_type")
            private Long partType;

            /**
             * 平台优惠退回金额
             */
            @JSONField(name = "platform_discount_return_amount")
            private Long platformDiscountReturnAmount;

            /**
             * 平台优惠退回状态，
             * 枚举：0:待退补贴；1:退补贴成功；2:退补贴失败
             */
            @JSONField(name = "platform_discount_return_status")
            private Long platformDiscountReturnStatus;

            /**
             * 换货、补寄时的收货四级地址（只有换货、补寄时，这个字段才会有值）
             */
            @JSONField(name = "post_address")
            private PostAddr postAddress;

            /**
             * 运费优惠退回金额
             */
            @JSONField(name = "post_discount_return_amount")
            private Long postDiscountReturnAmount;

            /**
             * 运费优惠退回状态，枚举：
             * 0:待退补贴；1:退补贴成功；2:退补贴失败
             */
            @JSONField(name = "post_discount_return_status")
            private Long postDiscountReturnStatus;

            /**
             * 实际退款金额;单位：分
             */
            @JSONField(name = "real_refund_amount")
            private Long realRefundAmount;

            /**
             * 申请原因
             */
            private String reason;

            /**
             * 原因码；
             * 通过【afterSale/rejectReasonCodeList】接口获取
             */
            @JSONField(name = "reason_code")
            private Long reasonCode;

            /**
             * 申请描述
             */
            @JSONField(name = "reason_remark")
            private String reasonRemark;

            /**
             * 用户申请售后选择的二级原因标签
             */
            @JSONField(name = "reason_second_labels")
            private List<ReasonSecondLabels> reasonSecondLabels;

            /**
             * 退款失败文案
             */
            @JSONField(name = "refund_fail_reason")
            private String refundFailReason;

            /**
             * 售后运费
             */
            @JSONField(name = "refund_post_amount")
            private Long refundPostAmount;

            /**
             * 退款补贴总金额
             */
            @JSONField(name = "refund_promotion_amount")
            private Long refundPromotionAmount;

            /**
             * 退款状态;
             * 1-待退款;2-退款中;3-退款成功;4退款失败;5追缴成功;
             */
            @JSONField(name = "refund_status")
            private Long refundStatus;

            /**
             * 售后总金额（含运费）
             */
            @JSONField(name = "refund_total_amount")
            private Long refundTotalAmount;

            /**
             * 退款方式
             */
            @JSONField(name = "refund_type")
            private Long refundType;

            /**
             * 退款方式文案
             */
            @JSONField(name = "refund_type_text")
            private String refundTypeText;

            /**
             * 卡券商品申请退款的张数
             */
            @JSONField(name = "refund_voucher_num")
            private Long refundVoucherNum;

            /**
             * 多次券商品申请退款的次数，对于单次券，此字段值与refund_voucher_num相同
             */
            @JSONField(name = "refund_voucher_times")
            private Long refundVoucherTimes;

            /**
             * 退货地址
             */
            @JSONField(name = "return_address")
            private PostAddr returnAddress;

            /**
             * 退货地址ID
             */
            @JSONField(name = "return_address_id")
            private Long returnAddressId;

            /**
             * 物流异常风控编码
             */
            @JSONField(name = "risk_decsison_code")
            private Long riskDecsisonCode;

            /**
             * 物流异常风控描述
             */
            @JSONField(name = "risk_decsison_description")
            private String riskDecsisonDescription;

            /**
             * 物流异常风控理由
             */
            @JSONField(name = "risk_decsison_reason")
            private String riskDecsisonReason;

            /**
             * 逾期时间
             */
            @JSONField(name = "status_deadline")
            private Long statusDeadline;

            /**
             * 门店id
             */
            @JSONField(name = "store_id")
            private String storeId;

            /**
             * 门店名称
             */
            @JSONField(name = "store_name")
            private String storeName;

            /**
             * 售后单更新时间
             */
            @JSONField(name = "update_time")
            private Long updateTime;

            /**
             * 换货SKU信息
             */
            @Data
            public static class ExchangeSkuInfo {
                /**
                 * 商品编码
                 */
                private String code;
                /**
                 * 商品名称
                 */
                private String name;
                /**
                 * 替换数量
                 */
                private Long num;

                /**
                 * 商家编号
                 */
                @JSONField(name = "out_sku_id")
                private String outSkuId;

                /**
                 * 区域库存仓ID
                 */
                @JSONField(name = "out_warehouse_id")
                private String outWarehouseId;

                /**
                 * 换货商品的价格，单位分
                 */
                private String price;

                /**
                 * 商品skuid
                 */
                @JSONField(name = "sku_id")
                private String skuId;

                /**
                 * sku规格信息
                 */
                @JSONField(name = "spec_desc")
                private String specDesc;
                /**
                 * sku外部供应商编码供应商ID
                 */
                @JSONField(name = "supplier_id")
                private String supplierId;

                /**
                 * 商品图片url
                 */
                private String url;
            }

            /**
             * 用户申请售后选择的二级原因标签
             */
            @Data
            public static class ReasonSecondLabels {
                /**
                 * 二级原因标签编码
                 */
                private Long code;
                /**
                 * 二级原因标签名称
                 */
                private String name;
            }

            /**
             * 售后标签
             */
            @Data
            public static class AftersaleTags {

                /**
                 * 标签悬浮文案的占位符定义
                 */
                private Placeholder placeholder;
                /**
                 * 标签名称
                 */
                @JSONField(name = "tag_detail")
                private String tagDetail;
                /**
                 * 标签关键字
                 */
                @JSONField(name = "tag_detail_en")
                private String tagDetailEn;

                /**
                 * 标签悬浮文案的占位符定义
                 */
                @JSONField(name = "tag_detail_text")
                private String tagDetailText;

                /**
                 * 标签跳转链接
                 */
                @JSONField(name = "tag_link_url")
                private String tagLinkUrl;

                /**
                 * 标签悬浮文案的占位符定义
                 */
                @Data
                public static class Placeholder {

                    /**
                     * 占位符文案
                     */
                    private String text;
                    /**
                     * 占位符跳转链接
                     */
                    private String url;
                }
            }
        }

        @Data
        public static class AfterSaleShopRemarks {

            /**
             * 备注关联的售后ID
             */
            @JSONField(name = "after_sale_id")
            private Long afterSaleId;
            /**
             * 创建时间
             */
            @JSONField(name = "create_time")
            private Long createTime;
            /**
             * 操作人
             */
            private String operator;
            /**
             * 备注关联的订单ID
             */
            @JSONField(name = "order_id")
            private String orderId;
            /**
             * 备注内容
             */
            private String remark;
        }

        /**
         * 仲裁信息
         */
        @Data
        public static class ArbitrateInfo {
            /**
             * 仲裁责任方
             */
            @JSONField(name = "arbitrate_blame")
            private Long arbitrateBlame;

            /**
             * 仲裁结果
             */
            @JSONField(name = "arbitrate_conclusion")
            private Long arbitrateConclusion;

            /**
             * 仲裁单创建时间
             */
            @JSONField(name = "arbitrate_create_time")
            private Long arbitrateCreateTime;

            /**
             * 仲裁证据
             */
            @JSONField(name = "arbitrate_evidence")
            private ArbitrateEvidenceTmpl arbitrateEvidence;

            /**
             * 凭证示例
             */
            @JSONField(name = "arbitrate_evidence_tmpl")
            private ArbitrateEvidenceTmpl arbitrateEvidenceTmpl;

            /**
             * 仲裁单id
             */
            @JSONField(name = "arbitrate_id")
            private String arbitrateId;

            /**
             * 仲裁原因
             */
            @JSONField(name = "arbitrate_opinion")
            private String arbitrateOpinion;

            /**
             * 仲裁状态
             */
            @JSONField(name = "arbitrate_status")
            private Long arbitrateStatus;

            /**
             * 仲裁截止时间
             */
            @JSONField(name = "arbitrate_status_deadline")
            private String arbitrateStatusDeadline;

            /**
             * 仲裁单更新时间
             */
            @JSONField(name = "arbitrate_update_time")
            private Long arbitrateUpdateTime;

            /**
             * 是否需要上传凭证
             */
            @JSONField(name = "is_required_evidence")
            private Boolean isRequiredEvidence;

            /**
             * 凭证示例
             */
            @Data
            public static class ArbitrateEvidenceTmpl {
                /**
                 * 示例截止时间
                 */
                @JSONField(name = "dead_line")
                private Long deadLine;

                /**
                 * 仲裁描述
                 */
                private String describe;
                /**
                 * 仲裁图片示例
                 */
                private List<String> images;

            }
        }

        /**
         * 物流信息
         */
        @Data
        public static class LogisticsInfo {
            /**
             * 卖家换货物流信息
             */
            private Order exchange;
            /**
             * 卖家发货正向物流信息
             */
            private List<Order> order;
            /**
             * 补寄物流
             */
            private Order resend;

            /**
             * 买家退货物流信息
             */
            @JSONField(name = "return")
            private Order ret;


            /**
             * 物流订单信息
             */
            @Data
            public static class Order {
                /**
                 *
                 * 物流公司编码
                 */
                @JSONField(name = "company_code")
                private String companyCode;
                /**
                 * 物流公司名称
                 */
                @JSONField(name = "company_name")
                private String companyName;
                /**
                 * 正向物流状态
                 */
                @JSONField(name = "logistics_state")
                private Long logisticsState;
                /**
                 * 物流状态到达时间
                 */
                @JSONField(name = "logistics_time")
                private Long logisticsTime;
                /**
                 * 物流单号
                 */
                @JSONField(name = "tracking_no")
                private String trackingNo;
                /**
                 * 增值服务标签
                 */
                @JSONField(name = "value_added_services")
                private List<ValueAddedServices> valueAddedServices;

                /**
                 * 增值服务标签
                 */
                @Data
                public static class ValueAddedServices {
                    /**
                     * 标签编码
                     */
                    private String code;
                    /**
                     * 标签名称
                     */
                    private String name;
                }
            }
        }

        /**
         * 价保详情
         */
        @Data
        public static class PriceProtectionDetail {

            /**
             * 核准价
             */
            @JSONField(name = "check_price")
            private Price checkPrice;

            /**
             * 平台价保回收金额
             */
            @JSONField(name = "platform_recycle_amount")
            private Long platformRecycleAmount;

            /**
             * 平台价保补贴商家金额进度状态，1表示成功
             */
            @JSONField(name = "platform_to_merchant_refund_status")
            private Long platformToMerchantRefundStatus;

            /**
             * 价保计算公式
             */
            @JSONField(name = "price_protection_formulas")
            private String priceProtectionFormulas;

            /**
             * 钱款承担方
             */
            @JSONField(name = "refund_bearer_list")
            private List<Price.Amount> refundBearerList;

            /**
             * 退款明细
             */
            @JSONField(name = "refund_detail")
            private Price refundDetail;

            /**
             * 基准价
             */
            @JSONField(name = "standard_price")
            private Price standardPrice;

            /**
             * 价保文案标题
             */
            private String title;

            /**
             * 核准价
             */
            @Data
            public static class Price {
                /**
                 * 总价
                 */
                @JSONField(name = "actual_amount")
                private Amount actualAmount;
                /**
                 * 减数明细
                 */
                @JSONField(name = "deduction_price_detail")
                private List<Amount> deductionPriceDetail;

                /**
                 * 原价
                 */
                @JSONField(name = "origin_amount")
                private Amount originAmount;

                /**
                 * 金额
                 */
                @Data
                public static class Amount {
                    /**
                     * 金额
                     */
                    private Long amount;
                    /**
                     * 金额明细
                     */
                    @JSONField(name = "price_text")
                    private String priceText;
                }

            }


        }

        /**
         * 售后标签
         */
        @Data
        public static class AfterSaleServiceTags {

            @JSONField(name = "after_sale_service_tag")
            List<AfterSaleServiceTag> afterSaleServiceTag;

            /**
             * 售后服务标签
             */
            @Data
            public static class AfterSaleServiceTag {
                /**
                 * 服务标签名称
                 */
                @JSONField(name = "tag_detail")
                private String tagDetail;

                /**
                 * 服务标签英文代号
                 */
                @JSONField(name = "tag_detail_en")
                private String tagDetailEn;

                /**
                 * 服务跳转地址
                 */
                @JSONField(name = "tag_link_url")
                private String tagLinkUrl;
            }
        }
    }
}
