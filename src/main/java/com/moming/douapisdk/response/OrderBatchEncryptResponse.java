package com.moming.douapisdk.response;

import com.alibaba.fastjson.annotation.JSONField;
import com.moming.douapisdk.BaseDouYinResponse;
import com.moming.douapisdk.domain.CustomErr;
import com.moming.douapisdk.domain.EncryptInfo;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

/**
 * 批量加密后的密文
 * @author LangYu
 * @date 2021/7/10
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class OrderBatchEncryptResponse  extends BaseDouYinResponse {

    /**
     * 加密之后的数据
     */
    @JSONField(name = "encrypt_infos")
    private List<EncryptInfo> encryptInfos;
    /**
     * 业务错误
     */
    @JSONField(name = "custom_err")
    private CustomErr customErr;

}
