package com.moming.douapisdk.response.product;

import com.moming.douapisdk.BaseDouYinResponse;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author lujingpo
 * @date 2022/5/13
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class ProductDetailResponse extends BaseDouYinResponse {
    private static final long serialVersionUID = -1532773510201791227L;


}
