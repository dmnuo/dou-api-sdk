package com.moming.douapisdk.response.product;

import com.alibaba.fastjson.annotation.JSONField;
import com.moming.douapisdk.BaseDouYinResponse;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @author lujingpo
 * @date 2022/1/20
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class ProductListV2Response extends BaseDouYinResponse {

    private static final long serialVersionUID = 6851550542077562003L;

    private Long page;
    private Long size;
    private Long total;
    private List<ProductList> data;


    @Data
    @NoArgsConstructor
    public static class ProductList {

        private CategoryDetail categoryDetail;
        /**
         * 商品审核状态：1未提审 2审核中 3审核通过 4审核驳回 5封禁
         */
        @JSONField(name = "check_status")
        private Long checkStatus;
        /**
         * 佣金比例
         */
        @JSONField(name = "cos_ratio")
        private Long cosRatio;
        /**
         * 商品创建时间
         */
        @JSONField(name = "create_time")
        private Long createTime;
        /**
         * 商品描述
         */
        @JSONField(name = "description")
        private String description;
        /**
         * 售价，单位分
         */
        @JSONField(name = "discount_price")
        private Long discountPrice;
        /**
         * 额外信息，如资质
         */
        private String extra;
        /**
         * 商品图片url
         */
        private String img;
        /**
         * 是否是组套商品
         */
        @JSONField(name = "is_package_product")
        private Boolean isPackageProduct;
        /**
         * 划线价，单位分
         */
        @JSONField(name = "market_price")
        private Long marketPrice;
        /**
         * 手机号
         */
        @JSONField(name = "mobile")
        private String mobile;
        /**
         * 商品名
         */
        private String name;
        /**
         * 推荐使用，外部商家编码，支持字符串和数字2种
         */
        @JSONField(name = "out_product_id")
        private Long outProductId;
        /**
         * 推荐使用，外部商家编码，支持字符串和数字2种
         */
        @JSONField(name = "outer_product_id")
        private String outerProductId;
        /**
         * 此商品关联的组套主商品ID
         */
        @JSONField(name = "package_product_list")
        private List<Long> packageProductList;
        /**
         * 支持的支付方式：0货到付款 1在线支付 2两者都支持
         */
        @JSONField(name = "pay_type")
        private Long payType;
        /**
         * 商品ID
         */
        @JSONField(name = "product_Id")
        private Long productId;
        /**
         * 0-普通，3-虚拟，6玉石闪购，7云闪购
         */
        @JSONField(name = "product_type")
        private Long productType;
        /**
         * 商家推荐语
         */
        @JSONField(name = "recommend_remark")
        private String recommendRemark;
        /**
         * 规格id
         */
        @JSONField(name = "spec_id")
        private Long specId;
        /**
         * 商品上下架状态：0上架 1下架
         */
        @JSONField(name = "status")
        private Long status;
        /**
         * 此商品关联的组套子商品ID
         */
        @JSONField(name = "sub_product_list")
        private List<Long> subProductList;
        /**
         * 商品更新时间
         */
        @JSONField(name = "update_time")
        private Long updateTime;
    }

    @Data
    @NoArgsConstructor
    public static class CategoryDetail {

        /**
         * 一级类目
         */
        @JSONField(name = "first_cid")
        private Long firstCid;

        /**
         * 二级类目
         */
        @JSONField(name = "second_cid")
        private Long secondCid;

        /**
         * 三级类目
         */
        @JSONField(name = "third_cid")
        private Long thirdCid;

        /**
         * 四级类目
         */
        @JSONField(name = "fourth_cid")
        private Long fourthCid;

        /**
         * 一级类目名称
         */
        @JSONField(name = "first_cname")
        private String firstCname;

        /**
         * 二级类目名称
         */
        @JSONField(name = "second_cname")
        private String secondCname;

        /**
         * 三级类目名称
         */
        @JSONField(name = "third_cname")
        private String thirdCname;

        /**
         * 四级类目名称
         */
        @JSONField(name = "fourth_cname")
        private String fourthCname;

    }
}
