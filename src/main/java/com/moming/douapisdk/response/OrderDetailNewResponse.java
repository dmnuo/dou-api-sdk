package com.moming.douapisdk.response;

import com.alibaba.fastjson.annotation.JSONField;
import com.moming.douapisdk.BaseDouYinResponse;
import com.moming.douapisdk.domain.*;
import lombok.Data;
import lombok.EqualsAndHashCode;


/**
 * @author lujingpo
 * @date 2021/6/12
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class OrderDetailNewResponse extends BaseDouYinResponse {
    private static final long serialVersionUID = 7086013006540258767L;

    /**
     * 订单详情
     */
    @JSONField(name = "shop_order_detail")
    private ShopOrder shopOrderDetail;

}
