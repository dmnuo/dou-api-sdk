package com.moming.douapisdk.response;

import com.moming.douapisdk.BaseDouYinResponse;

/**
 * 批量发货的结果返回值
 *
 *
 * 返回值只有成功和不成功
 * @author lujingpo
 * @date 2021/3/15
 */
public class LogisticsAddResponse extends BaseDouYinResponse {
    private static final long serialVersionUID = 6764188081725415218L;


}
