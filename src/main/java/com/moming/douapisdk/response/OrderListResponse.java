package com.moming.douapisdk.response;

import com.moming.douapisdk.BaseDouYinResponse;
import com.moming.douapisdk.domain.Order;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

/**
 * 订单检索接口返回值
 *
 * @author tianzong
 * @date 2020/7/21
 * @deprecated 1.1.0 废弃，使用{@link OrderSearchListResponse}
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class OrderListResponse extends BaseDouYinResponse {

    private static final long serialVersionUID = -7698845462133858817L;

    private Long total;

    private Long count;

    private List<Order> list;

}
