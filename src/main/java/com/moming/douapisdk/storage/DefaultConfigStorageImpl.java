package com.moming.douapisdk.storage;

import com.moming.douapisdk.properties.DouYinProperties;
import lombok.Data;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * 内存token缓存
 *
 * @author lujingpo
 * @date 2021/2/3
 */
public class DefaultConfigStorageImpl implements IDouYinConfigStorage{

    private volatile String accessToken;

    private volatile String refreshToken;

    private volatile long expiresRefreshTime;

    private volatile String appKey;

    private volatile String appSecret;

    private volatile String shopId;

    private volatile String apiUrl;

    private volatile long expiresTime;

    private volatile boolean autoRefresh;

    protected transient Lock accessTokenLock = new ReentrantLock();

    public DefaultConfigStorageImpl(DouYinProperties douYinProperties) {
        this.appKey = douYinProperties.getAppKey();
        this.appSecret = douYinProperties.getAppSecret();
        this.shopId = douYinProperties.getShopId();
        this.apiUrl = douYinProperties.getApiUrl();
        this.autoRefresh =  douYinProperties.isAutoRefresh();
    }

    public DefaultConfigStorageImpl() {

    }

    /**
     * 获取accessToken
     *
     * @return String
     */
    @Override
    public String getAccessToken() {
        return this.accessToken;
    }

    /**
     * 更新accessToken
     *  @param accessToken token
     * @param refreshToken 刷新令牌
     * @param expiresInSeconds   过期时间
     */
    @Override
    public synchronized void updateAccessToken(String accessToken, String refreshToken, int expiresInSeconds) {
        this.accessToken = accessToken;
        this.expiresTime = System.currentTimeMillis() + (expiresInSeconds - 200) * 1000L;
        this.refreshToken = refreshToken;
        this.expiresRefreshTime = System.currentTimeMillis() + 14* 24 * 60 * 60 * 1000L;
    }

    /**
     * 获取刷新令牌
     *
     * @return 刷新令牌
     */
    @Override
    public String getRefreshToken() {
        return this.refreshToken;
    }

    /**
     * 是否已经过期
     *
     * @return 是否过期
     */
    @Override
    public boolean isRefreshTokenExpired() {
        return System.currentTimeMillis() > this.expiresRefreshTime;
    }

    /**
     * 获取更新token的锁
     *
     * @return 锁
     */
    @Override
    public Lock getAccessTokenLock() {
        return this.accessTokenLock;
    }

    /**
     * 是否已经过期
     *
     * @return 是否过期
     */
    @Override
    public boolean isAccessTokenExpired() {
        return System.currentTimeMillis() > this.expiresTime;
    }

    /**
     * 获取appKey
     *
     * @return 结果
     */
    @Override
    public String getAppKey() {
        return appKey;
    }

    /**
     * 设置appKey
     *
     * @param appKey key
     */
    @Override
    public void setAppKey(String appKey) {
        this.appKey = appKey;
    }

    /**
     * 获取appSecret
     *
     * @return 结果
     */
    @Override
    public String getAppSecret() {
        return appSecret;
    }

    @Override
    public String getShopId() {
        return shopId;
    }

    @Override
    public void setShopId(String shopId) {
        this.shopId = shopId;
    }

    /**
     * 强制将access token过期掉.
     */
    @Override
    public void expireAccessToken() {
        this.expiresTime = 0;
    }

    /**
     * 设置apiUrl
     *
     * @param apiUrl key
     */
    @Override
    public void setApiUrl(String apiUrl) {
        this.apiUrl = apiUrl;
    }

    /**
     * 获取apiUrl
     *
     * @return 结果
     */
    @Override
    public String getApiUrl() {
        return this.apiUrl;
    }

    /**
     * 设置secret
     *
     * @param secret 密钥
     */
    @Override
    public void setAppSecret(String secret) {
        this.appSecret = secret;
    }

    /**
     * 是否自动刷新token
     *
     * @return .
     */
    @Override
    public boolean autoRefreshToken() {
        return this.autoRefresh;
    }


}
