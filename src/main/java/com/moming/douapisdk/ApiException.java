package com.moming.douapisdk;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.security.PrivilegedActionException;

/**
 * DouYin客户端异常
 *
 * @author tianzong
 * @date 2020/7/23
 */

@EqualsAndHashCode(callSuper = true)
@Data
public class ApiException extends Exception {

    private static final long serialVersionUID = 8585811867368040814L;


    /**
     * 错误码
     *
     * @deprecated 已废弃，使用code来替代
     */
    @JSONField(name = "err_no")
    private int errNo;

    /**
     * 错误信息
     * @deprecated 已废弃，使用msg来替代
     */
    private String message;

    private int code;

    @JSONField(name = "log_id")
    private String logId;

    private String msg;

    @JSONField(name = "sub_code")
    private String subCode;

    @JSONField(name = "sub_msg")
    private String subMsg;

    public ApiException(int errNo, String message) {
        this.code = errNo;
        this.msg = message;
    }

    public ApiException(int code, String msg, String subCode, String subMsg, String logId) {
        this.code = code;
        this.msg = msg;
        this.subCode = subCode;
        this.subMsg = subMsg;
        this.logId = logId;
    }

    /**
     * Constructs a new throwable with the specified cause and a detail
     * message of {@code (cause==null ? null : cause.toString())} (which
     * typically contains the class and detail message of {@code cause}).
     * This constructor is useful for throwables that are little more than
     * wrappers for other throwables (for example, {@link
     * PrivilegedActionException}).
     *
     * <p>The {@link #fillInStackTrace()} method is called to initialize
     * the stack trace data in the newly created throwable.
     *
     * @param cause the cause (which is saved for later retrieval by the
     *              {@link #getCause()} method).  (A {@code null} value is
     *              permitted, and indicates that the cause is nonexistent or
     *              unknown.)
     * @since 1.4
     */
    public ApiException(Throwable cause) {
        super(cause);
    }
}
