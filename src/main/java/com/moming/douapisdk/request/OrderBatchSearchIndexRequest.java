package com.moming.douapisdk.request;

import com.alibaba.fastjson.JSON;
import com.moming.douapisdk.BaseDouYinRequest;
import com.moming.douapisdk.domain.PlainText;
import com.moming.douapisdk.internal.util.DouYinHashMap;
import com.moming.douapisdk.response.OrderBatchSearchIndexResponse;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;
import java.util.Map;

/**
 * 批量获取索引
 * @author LangYu
 * @date 2021/7/10
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class OrderBatchSearchIndexRequest extends BaseDouYinRequest<OrderBatchSearchIndexResponse> {


    /**
     * 明文列表
     */
    private List<PlainText> plainTextList;


    @Override
    public String getApiUrl() {
        return "/order/batchSearchIndex";
    }

    @Override
    public String getApiMethodName() {
        return "order.batchSearchIndex";
    }

    @Override
    public Map<String, String> getTextParams() {
        final DouYinHashMap map = new DouYinHashMap();

        if (null != plainTextList) {
            map.put("plain_text_list", JSON.toJSONString(plainTextList));
        }

        return map;
    }
}
