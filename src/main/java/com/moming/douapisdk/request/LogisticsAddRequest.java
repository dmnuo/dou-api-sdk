package com.moming.douapisdk.request;

import com.alibaba.fastjson.annotation.JSONField;
import com.moming.douapisdk.BaseDouYinRequest;
import com.moming.douapisdk.internal.util.DouYinHashMap;
import com.moming.douapisdk.response.LogisticsAddResponse;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Map;

/**
 * 物流发货api接口
 *
 * @author lujingpo
 * @date 2021/3/15
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class LogisticsAddRequest extends BaseDouYinRequest<LogisticsAddResponse> {

    /**
     * 父订单ID，由orderList接口返回
     */
    @JSONField(name = "order_id")
    private String orderId;

    /**
     * 物流公司ID，由接口/order/logisticsCompanyList返回的物流公司列表中对应的ID
     */
    @JSONField(name = "logistics_id")
    private String logisticsId;

    /**
     * 物流公司名称
     */
    @JSONField(name = "company")
    private String company;

    /**
     * 运单号
     */
    @JSONField(name = "logistics_code")
    private String logisticsCode;

    /**
     * 获取api的url地址
     *
     * @return url地址
     */
    @Override
    public String getApiUrl() {
        return "/order/logisticsAdd";
    }

    /**
     * 获取API的名称
     *
     * @return API名称
     */
    @Override
    public String getApiMethodName() {
        return "order.logisticsAdd";
    }

    /**
     * 获取所有的key-value形式的文本请求参数集合，其中：
     * <ul>
     *     <li>Key: 请求参数名</li>
     *     <li>Value: 请求参数值</li>
     * </ul>
     *
     * @return 文本请求参数集合
     */
    @Override
    public Map<String, String> getTextParams() {
        DouYinHashMap params = new DouYinHashMap();
        params.put("order_id", this.getOrderId());
        params.put("logistics_id", this.getLogisticsId());
        params.put("company", this.getCompany());
        params.put("logistics_code", this.getLogisticsCode());

        return params;
    }
}
