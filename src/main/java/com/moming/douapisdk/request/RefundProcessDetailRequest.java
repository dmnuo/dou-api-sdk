package com.moming.douapisdk.request;

import com.moming.douapisdk.BaseDouYinRequest;
import com.moming.douapisdk.internal.util.DouYinHashMap;
import com.moming.douapisdk.response.RefundProcessDetailResponse;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Map;

/**
 * 根据子订单ID查询退款详情
 * 通过该接口，根据子订单ID查询退款详情信息
 * 1、订单未发货，买家申请整单退款
 * 2、订单已发货，买家申请发货后仅退款
 * 3、订单已发货，买家申请发货后退货
 * 4、订单已发货，买家申请换货
 * @author lujingpo
 * @date 2021/3/1
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class RefundProcessDetailRequest extends BaseDouYinRequest<RefundProcessDetailResponse> {

    private String orderId;

    /**
     * 获取api的url地址
     *
     * @return url地址
     */
    @Override
    public String getApiUrl() {
        return "/afterSale/refundProcessDetail";
    }

    /**
     * 获取API的名称
     *
     * @return API名称
     */
    @Override
    public String getApiMethodName() {
        return "afterSale.refundProcessDetail";
    }

    /**
     * 获取所有的key-value形式的文本请求参数集合，其中：
     * <ul>
     *     <li>Key: 请求参数名</li>
     *     <li>Value: 请求参数值</li>
     * </ul>
     *
     * @return 文本请求参数集合
     */
    @Override
    public Map<String, String> getTextParams() {
        DouYinHashMap textParams = new DouYinHashMap();
        textParams.put("order_id", this.orderId);
        return textParams;
    }
}
