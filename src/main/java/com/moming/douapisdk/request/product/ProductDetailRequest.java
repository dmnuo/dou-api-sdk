package com.moming.douapisdk.request.product;

import com.moming.douapisdk.BaseDouYinRequest;
import com.moming.douapisdk.response.product.ProductDetailResponse;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Map;

/**
 * @author lujingpo
 * @date 2022/5/13
 */

@Data
public class ProductDetailRequest extends BaseDouYinRequest<ProductDetailResponse> {



    /**
     * 获取api的url地址
     *
     * @return url地址
     */
    @Override
    public String getApiUrl() {
        return null;
    }

    /**
     * 获取API的名称
     *
     * @return API名称
     */
    @Override
    public String getApiMethodName() {
        return null;
    }

    /**
     * 获取所有的key-value形式的文本请求参数集合，其中：
     * <ul>
     *     <li>Key: 请求参数名</li>
     *     <li>Value: 请求参数值</li>
     * </ul>
     *
     * @return 文本请求参数集合
     */
    @Override
    public Map<String, String> getTextParams() {
        return null;
    }
}
