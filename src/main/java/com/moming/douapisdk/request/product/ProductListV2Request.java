package com.moming.douapisdk.request.product;

import com.moming.douapisdk.BaseDouYinRequest;
import com.moming.douapisdk.internal.util.DouYinHashMap;
import com.moming.douapisdk.internal.util.TimeUtils;
import com.moming.douapisdk.response.ProductListResponse;
import com.moming.douapisdk.response.product.ProductListV2Response;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.time.LocalDateTime;
import java.util.Map;

/**
 * 获取商品列表新版
 *
 * 获取商品列表详情，类似商家后台的商品列表
 *
 * @author lujingpo
 * @date 2022/1/20
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class ProductListV2Request extends BaseDouYinRequest<ProductListV2Response> {

    /**
     * 第几页（第一页为0）
     */
    private Long page;

    /**
     * 每页返回条数
     */
    private Long size;

    /**
     * 指定状态返回商品列表：0上架 1下架
     */
    private Long status;

    /**
     * 指定审核状态返回商品列表：
     * 1未提审 2审核中 3审核通过 4审核驳回 5封禁 7审核通过，待上架状态
     */
    private Long checkStatus;

    /**
     * 0-普通，3-虚拟，6玉石闪购，7云闪购
     */
    private Long productType;

    /**
     * 商品创建时间区间开始时间，格式为unix时间戳
     * 秒级时间戳
     */
    private LocalDateTime startTime;

    /**
     * 商品创建时间区间结束时间，格式为unix时间戳
     * 秒级时间戳
     */
    private LocalDateTime endTime;

    /**
     * 商品更新时间区间开始时间，格式为unix时间戳，不包括自身
     */
    private LocalDateTime updateStartTime;

    /**
     * 商品更新时间区间结束时间，格式为unix时间戳，不包括自身
     */
    private LocalDateTime updateEndTime;


    /**
     * 获取api的url地址
     *
     * @return url地址
     */
    @Override
    public String getApiUrl() {
        return "/product/listV2";
    }

    /**
     * 获取API的名称
     *
     * @return API名称
     */
    @Override
    public String getApiMethodName() {
        return "product.listV2";
    }

    /**
     * 获取所有的key-value形式的文本请求参数集合，其中：
     * <ul>
     *     <li>Key: 请求参数名</li>
     *     <li>Value: 请求参数值</li>
     * </ul>
     *
     * @return 文本请求参数集合
     */
    @Override
    public Map<String, String> getTextParams() {
        final DouYinHashMap map = new DouYinHashMap();
        map.put("status", status);
        map.put("check_status", checkStatus);
        map.put("product_type", productType);
        if (startTime != null) {
            map.put("start_time", TimeUtils.toEpochSecond(startTime));
        }
        if (endTime != null) {
            map.put("end_time", TimeUtils.toEpochSecond(endTime));
        }
        if (updateStartTime != null) {
            map.put("update_start_time", TimeUtils.toEpochSecond(updateStartTime));
        }
        if (updateEndTime != null) {
            map.put("update_end_time", TimeUtils.toEpochSecond(updateEndTime));
        }
        map.put("page", page);
        map.put("size", size);
        return map;
    }
}
