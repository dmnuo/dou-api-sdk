package com.moming.douapisdk.request;

import com.moming.douapisdk.BaseDouYinRequest;
import com.moming.douapisdk.domain.IRequestParameterEnum;
import com.moming.douapisdk.internal.util.DouYinHashMap;
import com.moming.douapisdk.response.TradeRefundListSearchResponse;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.time.LocalDateTime;
import java.util.Map;

/**
 * @author lujingpo
 * @date 2021/2/1
 * @deprecated 已废弃，请使用 {@link com.moming.douapisdk.request.aftersale.AfterSaleListRequest}
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class TradeRefundListSearchRequest extends BaseDouYinRequest<TradeRefundListSearchResponse> {

    /**
     * 订单号
     */
    private Long orderId;

    /**
     * 页数（默认值为0，第一页从0开始）
     */
    private Integer page;

    /**
     * 订单排序方式：最近的在前，1；最近的在后，0 默认1
     */
    private Integer isDesc;

    /**
     * 排序字段，apply_time 售后申请时间
     * update_time 售后信息更新时间
     * 默认apply_time
     */
    private ORDER_BY orderBy;

    /**
     * 每页订单数（默认为10，最大100）
     */
    private Integer size;

    /**
     * 售后申请开始时间
     */
    private LocalDateTime startTime;

    /**
     * 售后申请结束时间
     */
    private LocalDateTime endTime;

    /**
     * 售后状态 1: 所有,
     * 2: PreSaleAllAudit,发货前退款待处理
     * 3: RefundAudit,发货后仅退款待处理
     * 4: ReturnAudit,退货待处理
     * 5: ExchangeAudit,换货待处理
     * 6: RefundFail,同意退款、
     * 退款失败 7: AuditRefund,同意退款、退款成功
     * 8: AfterSaleAudit,待商家处理
     * 9: ReturnReceive,待商家收货
     * 10: AuditRefunding,同意退款，退款中
     * 11: ArbitratePending,仲裁中
     * 12: Close,售后关闭
     * 13: ReturnShip,待买家退货
     * 14: WaitBuyerReceive,换货待用户收货
     * 15: ExchangeSuccess,换货成功
     * 16: Refuse,拒绝售后
     * 17: ReceiveRefuse,退货后拒绝退款
     * 18: UploadRefund,待商家上传退款凭证
     */
    private Integer status;

    /**
     * 0: TypeReturn, 退货
     * 1: TypeRefund, 售后仅退款
     * 2: TypePreSaleAll, 售前退款
     * 3: TypeExchange, 换货
     * 5: TypeAll, 所有
     */
    private Integer type;

    /**
     * 1: LogisticsStatusAll, 所有
     * 2: Shipped, 已发货
     * 3: NotShipped, 未发货
     */
    private Integer logisticsStatus;

    /**
     * 售后id
     */
    private Long afterSaleId;

    /**
     * 支付方式 1: PayTpeAll, 所有
     * 2: Cod, 货到付款
     * 3: Online, 在线支付
     */
    private Integer payType;

    /**
     * 仲裁状态 1: ArbitrationStatusAll, 所有
     * 2: ArbitrationStatusNone, 无仲裁
     * 3: Pending, 仲裁中
     * 4: Finished,仲裁结束
     */
    private Integer arbitrateStatus;

    /**
     * 退款类型 0: Origin, 原路退回
     * 1: Offline, 线下退款
     * 2: Impurest,预付款退款
     * 3: Pledge,保证金退款
     * 4: None,无需退款
     * 5: RefundTypeAll, 所有
     */
    private Integer refundType;

    /**
     * 物流单号
     */
    private Long logisticsCode;


    /**
     * 获取api的url地址
     *
     * @return url地址
     */
    @Override
    public String getApiUrl() {
        return "/trade/refundListSearch";
    }

    /**
     * 获取API的名称
     *
     * @return API名称
     */
    @Override
    public String getApiMethodName() {
        return "trade.refundListSearch";
    }

    /**
     * 获取所有的key-value形式的文本请求参数集合，其中：
     * <ul>
     *     <li>Key: 请求参数名</li>
     *     <li>Value: 请求参数值</li>
     * </ul>
     *
     * @return 文本请求参数集合
     */
    @Override
    public Map<String, String> getTextParams() {
        DouYinHashMap textParams = new DouYinHashMap();
        textParams.put("order_id", this.orderId);
        textParams.put("page", this.page);
        textParams.put("is_desc", this.isDesc);
        textParams.put("order_by", this.orderBy);
        textParams.put("size", this.size);
        textParams.put("start_time", this.startTime);
        textParams.put("end_time", this.endTime);
        textParams.put("status", this.status);
        textParams.put("type", this.type);
        textParams.put("logistics_status", this.logisticsStatus);
        textParams.put("aftersale_id", this.afterSaleId);
        textParams.put("pay_type", this.payType);
        textParams.put("arbitrate_status", this.arbitrateStatus);
        textParams.put("refund_type", this.refundType);
        textParams.put("logistics_code", this.logisticsCode);
        return textParams;
    }

    public enum ORDER_BY implements IRequestParameterEnum<String> {
        /**
         * 售后申请时间
         *
         * 默认
         */
        APPLY_TIME("apply_time"),
        /**
         * 售后信息更新时间
         */
        UPDATE_TIME("update_time");

        ORDER_BY(String value) {
            this.value = value;
        }

        private final String value;

        /**
         * 枚举参数必须具有该返回值
         *
         * @return 结果
         */
        @Override
        public String getValue() {
            return this.value;
        }
    }
}
