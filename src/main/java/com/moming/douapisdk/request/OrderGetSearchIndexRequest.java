package com.moming.douapisdk.request;

import com.moming.douapisdk.BaseDouYinRequest;
import com.moming.douapisdk.internal.util.DouYinHashMap;
import com.moming.douapisdk.response.OrderGetSearchIndexResponse;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Map;

/**
 * 获取订单加密索引串
 * @author LangYu
 * @date 2021/7/10
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class OrderGetSearchIndexRequest extends BaseDouYinRequest<OrderGetSearchIndexResponse> {

    /**
     *电话号码
     */
    private String plainText;

    /**
     * 加密类型；
     * 1地址加密
     * 2姓名加密
     * 3电话加密
     */
    private Integer sensitiveType;

    @Override
    public String getApiUrl() {
        return "/order/getSearchIndex";
    }

    @Override
    public String getApiMethodName() {
        return "order.getSearchIndex";
    }

    @Override
    public Map<String, String> getTextParams() {
        final DouYinHashMap map = new DouYinHashMap();
        map.put("plain_text", plainText);
        map.put("sensitive_type", sensitiveType);
        return map;
    }
}
