package com.moming.douapisdk.request.aftersale;

import com.moming.douapisdk.BaseDouYinRequest;
import com.moming.douapisdk.internal.util.DouYinHashMap;
import com.moming.douapisdk.response.aftersale.AfterSaleDetailResponse;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Map;

/**
 * @author lujingpo
 * @date 2022/8/23
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class AfterSaleDetailRequest extends BaseDouYinRequest<AfterSaleDetailResponse> {

    /**
     * 售后单ID
     */
    private String afterSaleId;

    /**
     * 获取api的url地址
     *
     * @return url地址
     */
    @Override
    public String getApiUrl() {
        return "/afterSale/Detail";
    }

    /**
     * 获取API的名称
     *
     * @return API名称
     */
    @Override
    public String getApiMethodName() {
        return "afterSale.Detail";
    }

    /**
     * 获取所有的key-value形式的文本请求参数集合，其中：
     * <ul>
     *     <li>Key: 请求参数名</li>
     *     <li>Value: 请求参数值</li>
     * </ul>
     *
     * @return 文本请求参数集合
     */
    @Override
    public Map<String, String> getTextParams() {
        DouYinHashMap map = new DouYinHashMap();
        map.put("after_sale_id", afterSaleId);
        return map;
    }
}
