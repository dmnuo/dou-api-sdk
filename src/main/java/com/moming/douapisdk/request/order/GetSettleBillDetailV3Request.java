package com.moming.douapisdk.request.order;

import com.moming.douapisdk.BaseDouYinRequest;
import com.moming.douapisdk.internal.util.DouYinHashMap;
import com.moming.douapisdk.response.order.GetSettleBillDetailV3Response;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.util.CollectionUtils;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * 商家结算账单
 *
 * 已结算的订单才会有数据，数据T+1生成，建议第二天12点之后查询。如因任务积压导致延迟的情况，建议重试。
 *
 * @author lujingpo
 * @date 2022/7/6
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class GetSettleBillDetailV3Request extends BaseDouYinRequest<GetSettleBillDetailV3Response> {

    private final static DateTimeFormatter TIME_FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

    /**
     * 查询页大小(默认100,最大为1000)
     */
    private Long size;

    /**
     * 查询开始时间，格式为：yyyy-MM-dd HH:mm:ss，
     * 订单号未传的情况下，开始时间必须传，注意：分页查询时，除首次查询外，应填入上一次返回的next_start_time
     */
    private LocalDateTime startTime;

    /**
     * 查询结束时间，和end_time的时间间隔建议不超过7天，格式为：yyyy-MM-dd HH:mm:ss，
     * 订单号未传的情况下，结束时间必须传
     */
    private LocalDateTime endTime;

    /**
     * SKU单，子订单号，支持通过英文逗号分隔传入多个参数
     */
    private List<String> orderId;

    /**
     * 商品id
     */
    private String productId;

    /**
     * 结算账户，不传则
     * 默认为全部，枚举：
     * 1（微信：升级前）、
     * 2（微信）、
     * 3（支付宝）、
     * 4（合众支付）、
     * 5（聚合账户），
     * 支持通过英文逗号分隔传入多个参数
     */
    private String payType;

    /**
     * 业务类型，不传则默认为全部，枚举：
     * 1（鲁班广告）、
     * 2（值点商城）,
     * 3（精选联盟）、
     * 4（小店自卖）
     */
    private String flowType;

    /**
     * 时间类型 ，不传则默认为结算时间，枚举： 0（结算时间） 1（下单时间）
     */
    private String timeType;

    /**
     * 查询开始索引，注意：
     * 分页查询时，除首次查询可不填外，
     * 应填入上一次返回的next_start_index
     */
    private String startIndex;

    /**
     * 获取api的url地址
     *
     * @return url地址
     */
    @Override
    public String getApiUrl() {
        return "/order/getSettleBillDetailV3";
    }

    /**
     * 获取API的名称
     *
     * @return API名称
     */
    @Override
    public String getApiMethodName() {
        return "order.getSettleBillDetailV3";
    }

    /**
     * 获取所有的key-value形式的文本请求参数集合，其中：
     * <ul>
     *     <li>Key: 请求参数名</li>
     *     <li>Value: 请求参数值</li>
     * </ul>
     *
     * @return 文本请求参数集合
     */
    @Override
    public Map<String, String> getTextParams() {
        final DouYinHashMap map = new DouYinHashMap();
        map.put("size", size);
        if (Objects.nonNull(startTime)) {
            map.put("start_time", startTime.format(TIME_FORMATTER));
        }
        if (Objects.nonNull(endTime)) {
            map.put("end_time", endTime.format(TIME_FORMATTER));
        }
        if (!CollectionUtils.isEmpty(orderId)) {
            map.put("order_id", String.join(",", orderId));
        }

        map.put("product_id", productId);
        map.put("pay_type", payType);
        map.put("flow_type", flowType);
        map.put("time_type", timeType);
        map.put("start_index", startIndex);
        return map;
    }
}
