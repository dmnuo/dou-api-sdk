package com.moming.douapisdk.request.oauth;

import com.moming.douapisdk.BaseDouYinRequest;
import com.moming.douapisdk.domain.IRequestParameterEnum;
import com.moming.douapisdk.internal.util.DouYinHashMap;
import com.moming.douapisdk.response.OauthResponse;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Map;

/**
 * @author lujingpo
 * @date 2021/7/7
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class TokenCreateRequest extends BaseDouYinRequest<OauthResponse> {

    /**
     * 授权码
     * <p/>
     * 自用型应用可以传0,但是不能不传
     */
    private String code;

    /**
     * 授权方式
     */
    private GrantTypeEnum grantType;

    /**
     * 店铺id
     * <p/>
     * 店铺ID，仅自用型应用有效；
     * 若不传，则默认返回最早授权成功店铺对应的token信息
     */
    private String shopId;

    /**
     * 获取api的url地址
     *
     * @return url地址
     */
    @Override
    public String getApiUrl() {
        return "/token/create";
    }

    /**
     * 获取API的名称
     *
     * @return API名称
     */
    @Override
    public String getApiMethodName() {
        return "token.create";
    }

    /**
     * 获取所有的key-value形式的文本请求参数集合，其中：
     * <ul>
     *     <li>Key: 请求参数名</li>
     *     <li>Value: 请求参数值</li>
     * </ul>
     *
     * @return 文本请求参数集合
     */
    @Override
    public Map<String, String> getTextParams() {
        DouYinHashMap params = new DouYinHashMap();
        params.put("code", this.code);
        params.put("grant_type", this.grantType);
        params.put("shop_id", this.shopId);

        return params;
    }

    /**
     * 授权类型
     * <p/>
     * <blockquote>
     *     授权的类型
     *     <ul>
     *         <li>authorization_code</li>
     *         <li>authorization_self</li>
     *     </ul>
     * </blockquote>
     */
    public enum GrantTypeEnum implements IRequestParameterEnum<String> {

        /**
         * 工具型应用授权
         */
        AUTHORIZATION_CODE("authorization_code"),

        /**
         * 自用型应用授权
         */
        AUTHORIZATION_SELF("authorization_self");

        private final String value;

        GrantTypeEnum(String value) {
            this.value = value;
        }

        /**
         * 枚举参数必须具有该返回值
         *
         * @return 结果
         */
        @Override
        public String getValue() {
            return value;
        }
    }
}
