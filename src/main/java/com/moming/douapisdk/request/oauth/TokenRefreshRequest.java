package com.moming.douapisdk.request.oauth;

import com.moming.douapisdk.BaseDouYinRequest;
import com.moming.douapisdk.internal.util.DouYinHashMap;
import com.moming.douapisdk.response.OauthResponse;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Map;

/**
 * 使用刷新令牌刷新token
 *
 * @author lujingpo
 * @date 2021/7/7
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class TokenRefreshRequest extends BaseDouYinRequest<OauthResponse> {

    /**
     * 刷新令牌
     */
    private String refreshToken;

    /**
     * 刷新token的固定类型
     */
    private final static String GRANT_TYPE = "refresh_token";

    /**
     * 获取api的url地址
     *
     * @return url地址
     */
    @Override
    public String getApiUrl() {
        return "/token/refresh";
    }

    /**
     * 获取API的名称
     *
     * @return API名称
     */
    @Override
    public String getApiMethodName() {
        return "token.refresh";
    }

    /**
     * 获取所有的key-value形式的文本请求参数集合，其中：
     * <ul>
     *     <li>Key: 请求参数名</li>
     *     <li>Value: 请求参数值</li>
     * </ul>
     *
     * @return 文本请求参数集合
     */
    @Override
    public Map<String, String> getTextParams() {
        DouYinHashMap params = new DouYinHashMap();
        params.put("grant_type", GRANT_TYPE);
        params.put("refresh_token", this.refreshToken);

        return params;
    }
}
