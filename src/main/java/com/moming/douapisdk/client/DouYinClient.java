package com.moming.douapisdk.client;

import com.moming.douapisdk.ApiException;
import com.moming.douapisdk.BaseDouYinResponse;
import com.moming.douapisdk.DouYinRequest;

/**
 * 抖音API客户端
 * <p/>
 * 接口类
 *
 * 提供接口调用方法及token的获取方法
 *
 * <p>具体实现：</p>
 * <ul>
 *     <li>工具型应用：{@link DefaultDouYinClient}</li>
 *     <li>自研型应用：{@link SelfUseDouYinClient}</li>
 * </ul>
 *
 *
 * @author tianzong
 * @date 2020/7/23
 */
public interface DouYinClient {

    /**
     * 获取token
     * @return string
     */
    String getAccessToken();

    /**
     * 获取accessToken
     * @param forceRefresh 是否强制刷新
     * @return String
     */
    String getAccessToken(boolean forceRefresh);

    /**
     * 执行隐私API请求。
     * @param request 具体的API响应类
     * @param <T> 具体的API响应类
     * @return 具体的API响应
     * @throws ApiException API调用异常
     */
    <T extends BaseDouYinResponse> T execute(DouYinRequest<T> request) throws ApiException;

    /**
     * 执行隐私API请求。
     * @param request 具体的API响应类
     * @param accessToken 用户授权码
     * @param <T> 具体的API响应类
     * @return 具体的API响应
     * @throws ApiException API调用异常
     */
     <T extends BaseDouYinResponse> T execute(DouYinRequest<T> request, String accessToken) throws ApiException;

}
